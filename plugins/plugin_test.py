import logging

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

from ctxsearch.plugin_base import PluginBase

class TestPlugin(PluginBase):


    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if not "test" in action: return
        logging.debug(f"prepare_menu... {action}")

        if "delay" in action:
            def ola():
                menuitem = Gtk.MenuItem(label=f"Test delayed {action['test']}")
                add_menuitem_callback(menuitem)
            GLib.timeout_add(action["delay"], ola)
        else:
            # menuitem = Gtk.MenuItem()
            # img = Gtk.Image.new_from_file("/home/daniel/comp/CtxSearch/src0/ctxsearch/_test1.jpg")
            # img.show()
            # menuitem.add(img)

            menuitem = Gtk.MenuItem(label=f"Test {action['test']}")
            #menuitem.set_label("LAbel2")
            add_menuitem_callback(menuitem)



