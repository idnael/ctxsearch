import logging
import os, subprocess
import favicon
import tempfile
from threading import Thread
import urllib
import requests

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib
from gi.repository.GdkPixbuf import Pixbuf

from ctxsearch.plugin_base import PluginBase
from ctxsearch.globals import STATE_DIR

# After this time, I will try to download again a favicon from a site
FAVICON_EXPIRE_DAYS = 14

# melhor png pq suporta transparencia
ICON_EXTENSION = ".png"

# faz download do faviicon da pagina indicada e grava em file
# converte formato imagem se necesario
# chama callback quando pronto
# TODO cuidado: se falhou 1 vez, nao deve tentar sempre!
def download_favicon(url, icon_file, callback):
    #import time
    #time.sleep(3)

    try:
        logging.debug(f"download_favicon for url={url}")

        icons = favicon.get(url)
        if len(icons) ==0:
            # # save the favicon, even if empty
            # # If empty, that will prevent from trying to download over again
            with open(icon_file, "wb"):
                pass
            callback()

        icon = icons[0]

        if "." + icon.format == ICON_EXTENSION:
            # already in desired format

            # o verify=False era para evitar estes erros, com sites com self signed certificates, exemplo: www.corpusdoportugues.org.
            # Mas acontecem na mesma:
            # requests.exceptions.SSLError: HTTPSConnectionPool(host='www.corpusdoportugues.org', port=443): Max retries exceeded with url: /now/ (Caused by SSLError(SSLCertVerificationError(1, '[SSL: CERTIFICATE_VERIFY_FAILED] certificate verify failed: unable to get local issuer certificate (_ssl.c:1131)')))
            response = requests.get(icon.url, stream=True, verify=False)
            with open(icon_file, "wb") as fd:
                for chunk in response.iter_content(1024):
                    fd.write(chunk)

        else:
            # cria ficheiro temporaria com a extensao do icon para fazer download
            tmp =tempfile.mkstemp(suffix= "."+icon.format)[1]
            response = requests.get(icon.url, stream=True, verify=False)
            with open(tmp, "wb") as fd:
                for chunk in response.iter_content(1024):
                    fd.write(chunk)

            # TODO could do this with PIL instead of using external command
            # o "[0]" é pq alguns .ico tem varias imagnes... e entao o convert iria adicionar os sufixos -1, -2 etc...
            cmd = ["convert", tmp +"[0]", icon_file]
            status = subprocess.Popen(cmd).wait()
            if status != 0:
                # file won't be created
                logging.debug("convert error!")

    except:
        # error downloading favicon...
        pass

    callback()

MENU_FAVICON_SIZE = 20

class UrlPlugin(PluginBase):
    # dependencies = ["syntax"]

    action_methods = {
        (str, "urlquote"): lambda s: urllib.parse.quote(s), # TODO se s for None... retorna None .. .em vezde erro esquisito
        (str, "jsquote"): lambda s: s.replace('\\', '\\\\').replace('"', '\\"'),
    }

    def __init__(self, ctxsearch):
        PluginBase.__init__(self, ctxsearch)

        self.favicons_dir = os.path.join(STATE_DIR, "favicons")
        os.makedirs(self.favicons_dir, exist_ok=True)

    def _add_icon_to_menuitem(self, menuitem, icon_file):
        if os.path.exists(icon_file) and os.path.getsize(icon_file) != 0:
            try:
                pixbuf = Pixbuf.new_from_file_at_size(icon_file, MENU_FAVICON_SIZE, MENU_FAVICON_SIZE)
                img = Gtk.Image()
                img.set_from_pixbuf(pixbuf)
                menuitem.set_image(img)
            except:
                # delete it? empty it?
                menuitem.set_image(None)

        else:
            menuitem.set_image(None)

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        # logging.debug(f"prepare_menu... action={action}, ctx={ctx}")

        # vou aceitar os actions que tem o elemento "url", que é o url
        # nota: no ficheiro de configuracao, serao usados urls do tipo
        # http://pt.wikipedia.org/wiki/${text__quote}
        # Mas O modsyntax já terá feito as substituicoes portanto eu vou reeber o url final

        if "url" not in action:
            return

        if "label" not in action:
            raise Exception("missing label")

        # logging.debug("__ menuitem... label=%s" % action["label"])
        menuitem = Gtk.ImageMenuItem(label=action.get("label", ctx))
        add_menuitem_callback(menuitem)

        self.ctxsearch.prepare_click_to_open_url(menuitem, action.get("url", ctx), ctx, action)

        if "icon" in action:
            # is relative to config file folder
            # TODO devia ser num thread?
            icon_file = os.path.join(self.config.cfg_dir, action.get("icon", ctx))
            self._add_icon_to_menuitem(menuitem, icon_file)

        else:
            # If url specified in config file is
            # https://www.google.pt/search?q=${text__quote}
            # then the real url will be always different
            # So, I Use only the base url
            # TODO problem? a site can have different favicons depending on the path?
            # TODO: if I could use the original url from the config file, and grab the favicon for "https://www.google.pt/search?q=${text__quote}" ??
            # or replace all {xx} with "" ?

            ## 20211002 NAO FAÇO substituicoes aqui!!! will be None in some cases, like "open location" action
            url =action.get("url", ctx)
            if url:
                #print(f"___ url {url}")
                split_url = urllib.parse.urlsplit(url)
                #print(f"___ split_url {split_url}")
                icon_file = os.path.join(self.favicons_dir, urllib.parse.quote_plus(split_url.scheme+"://"+split_url.netloc))

                # check if already downloaedd
                if os.path.exists(icon_file):
                    self._add_icon_to_menuitem(menuitem, icon_file)

                else:
                    def favicon_ready():
                        GLib.idle_add(self._add_icon_to_menuitem, menuitem, icon_file)

                    # TODO uma forma de dizer que nao quero replace? - acdedo diratamente ao dict
                    Thread(target=lambda: download_favicon(action.dict.get("url"), icon_file, favicon_ready)).start()
