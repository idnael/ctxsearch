import subprocess, logging, io
from threading import Thread

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

from ctxsearch.plugin_base import PluginBase

class CommandPlugin(PluginBase):

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if not "command" in action: return
        logging.debug(f"prepare_menu... {action}")

        menuitem = Gtk.MenuItem(label=action.get("label", ctx))

        #fun doit
        def doit():
            logging.debug("clicked!")

            # if the command does not exists, this will not raise an exception, because it is a shell subprocess

            cmd = action.get("command", ctx)
            if "command_input" in action and action.get("command_input", ctx):
                #print "ACTI?",action["command_input"], type(action["command_input"])

                proc = subprocess.Popen(cmd, shell=True, stdin=subprocess.PIPE)

                w = io.TextIOWrapper(proc.stdin, encoding='utf8')
                w.write(ctx["text"] + "\n")
                w.close()

            else:
                proc = subprocess.Popen(cmd, shell=True)

            status = proc.wait()
            logging.debug(f"status={status}")
            if status!=0:
                self.ctxsearch.error_message(f"Command returned exit status {status}")

        menuitem.connect('activate', lambda x: Thread(target=doit).start())

        add_menuitem_callback(menuitem)


