
import subprocess, psutil

import gi
gi.require_version('Wnck', '3.0')
from gi.repository import GLib, Wnck, Gtk
import logging

# TODO e se tivert multiplos profiles? como saber se o chrome que detectei é do meu profile?
#     ao abrir ele vai usar qq profile que eesteja no workspace!?

from ctxsearch.plugin_base import PluginBase

CHROME_TITLE = "Google Chrome"

# returns boolean indicating if there is a google chrome instance on current workspace
def is_in_current_workspace():
    screen = Wnck.Screen.get_default()
    screen.force_update()

    wrk = screen.get_active_workspace()
    #print(wrk.get_layout_column(), wrk.get_layout_row())

    for win in screen.get_windows():
        if win.get_workspace() == wrk:
            title = win.get_name()
            # google chrome titles are like this:
            # "<title defined by html page> - Google Chrome"

            if title.endswith(CHROME_TITLE):
                return True

            # each chrome window has his own application... Not useful
            #app = win.get_application()
            #print(app.get_name())

            # I could also use the pid
            # cmdline will be
            # ['/opt/google/chrome/chrome --profile-directory=Default --enable-crashpad']
            # why the list is it not split in several items?
            #pid = win.get_pid()
            #proc = psutil.Process(pid)
            #print(proc.cmdline())

    return False

class ChromePlugin(PluginBase):

    def does_open_url(self):
        return True

    def open_url(self, url, ctx, action):
        # launch process and ignore result
        if is_in_current_workspace():
            logging.debug("using existing chrome")
            # there is already a chrome instance in the current workspace
            # the url will be opened there
            subprocess.Popen(["google-chrome", url])
        else:
            logging.debug("opening new chrome")
            # create a new chrome instance, that will open in current workspace
            subprocess.Popen(["google-chrome", "--new-window", url])

if __name__ == "__main__" :
    # run like this: ./test.sh plugin_chrome
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    print(is_in_current_workspace())
