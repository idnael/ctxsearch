import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

import logging, re

from ctxsearch.plugin_base import PluginBase

RE_KOTLIN_NUMBERS = re.compile(r'[0-9_]+')

class EvalPlugin(PluginBase):

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if not "expr" in action:
            return

        try:
            expr = ctx["text"].strip()

            # replace kotlin numbers: 123_000 to 123000
            expr = RE_KOTLIN_NUMBERS.sub(lambda m: m.group(0).replace("_", ""), expr)

            result = eval(expr)
            logging.debug(f"expr={expr}, result={result}")

            result_str = str(result)

            # Don't show if it is the same
            if result_str == expr:
                return

            html = f"<i>{expr} = </i><b>{result_str}</b>"

            label = Gtk.Label()
            label.set_markup(html)

            hbox = Gtk.HBox()
            hbox.pack_start(label, expand=False, fill=False, padding=0)

            mi = Gtk.MenuItem()
            mi.add(hbox)
            mi.show_all()
            #mi.set_sensitive(False) # don't like gray...

            add_menuitem_callback(mi)

        except Exception as ex:
            # if selected text is not a valid expr, just ignore it!
            # logging.debug(f"error eval: {ex}")
            pass
