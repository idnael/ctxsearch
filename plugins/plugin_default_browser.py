import subprocess
import logging

from ctxsearch.plugin_base import PluginBase

# open url in external browser
class DefaultBrowserPlugin(PluginBase):

    dependencies = ["url"]

    # def __init__(self, ctxsearch):
    #     PluginBase.__init__(self, ctxsearch)
    #     logging.debug("BrowserPlugin constructor")

    def does_open_url(self):
        return True

    def open_url(self, url, ctx, action):
        subprocess.Popen(["xdg-open", url])

