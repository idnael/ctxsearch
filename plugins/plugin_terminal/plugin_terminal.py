import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib, Gio
from gi.repository.GdkPixbuf import Pixbuf

import io, urllib, subprocess, os, time, re, sys, psutil
import logging
import tempfile

from ctxsearch.plugin_base import PluginBase

from .lsparser import lsparse

from . import file_ops


# 20220101
# COPIADO de Doques
# recebe lista de folders, abssolutos, podem ou nao ter "/" no fim
# nota: folders nao precisam existir realmente
# valor retorno nao tem "/" no fim
# EXemplos:
# folders_common_ancestor('/daniel/together' '/daniel/together/arquivo') dá '/daniel/together'
# folders_common_ancestor('/daniel/together/ar' '/daniel/together/arquivo') dá '/daniel/together' - caso especial com o os.path.commonprefix
def folders_common_ancestor(folders):
    x = os.path.commonprefix(folders)
    if all(f == x or f.startswith(x+"/") for f in folders):
        # remove trailing "/" if exists
        return os.path.abspath(x)

    # 20211204 o os.path.commonprefix nao funciona exactamente como eu esperava...
    # EXEMPLO: os.path.commonprefix(["/home/daniel/comp/android/documentos/android_user", "/home/daniel/comp/android/danitools/docs"])
    # dá '/home/daniel/comp/android/d' -- que não é um folder valido...
    p = x.rindex("/")
    # tira o "/" no fim, que o commonprefix deixa...

    # tira o "/" no fim, que o commonprefix deixa...
    return x[:p]


# nota: ficheiros nao podem ter espacos no inicvio ou fim!
def format_size(bytes):
    K = 1024
    if bytes < 10*K:
        return "" + str(bytes) + " B"
    elif bytes < 10*K*K:
        return "" + str(bytes/K) + " KB"
    else:
        return "" + str(bytes/K/K) + " MB"

# the opposite of os.path.expanduser
def path_replace_user(path):
    home = os.getenv("HOME")
    if path == home:
        return "~"
    elif path.startswith(home+"/"):
        return "~" + path[len(home) :]
    else:
        return path

PREVIEW_MAX_SIZE = 200

# returns a Gtk.Image object with preview from file
# returns None if fails
# TODO
def preview_image(file):
    tmp_file = None
    try:
        tmp_file = tempfile.mkstemp(suffix=".jpg")[1]
        # this will work for pdf... not all!
        if subprocess.Popen(["convert", file, tmp_file]).wait() == 0:
            logging.debug("convert ok...")
            pixbuf = Pixbuf.new_from_file_at_size(tmp_file, PREVIEW_MAX_SIZE, PREVIEW_MAX_SIZE)
            img = Gtk.Image()

            logging.debug(f"img: {pixbuf.get_width()}x{pixbuf.get_height()}")
            img.set_from_pixbuf(pixbuf)
            return img
        else:
            return None
    except:
        return None

    finally:
        if tmp_file and os.path.exists(tmp_file):
            try:
                os.remove(tmp_file)
            except:
                pass

def files_description(basedir, files):
    count_dirs = 0
    total_size = 0
    for file in files:
        if os.path.isdir(file):
            count_dirs += 1
        elif os.path.isfile(file):
            total_size += os.path.getsize(file)
    count_files = len(files) - count_dirs

    res = ""
    if count_files:
        #if count_files == 1 and count_dirs == 0:
        #    res += filetools.file_description(files[0])
        #else:
        res += str(count_files) + " file" + (count_files != 1 and "s" or "")

        res += " (" + format_size(total_size) + ")"

    if count_dirs:
        if res: res += " and "
        res += str(count_dirs) + " directory" + (count_dirs != 1 and "s" or "")

    res += " in " + path_replace_user(basedir)
    logging.debug("RE", res)
    return (res)

# return TUPLE
# first element is Gio.AppInfo for default app to open all the given files, or None if it is not always the same default app.
# second element is list of Gio.AppInfo that can read all the given files, should include default_app also
def get_apps_for_files(files):
    all_apps_result = None
    default_app_result = None

    for file in files:
        content_type, certain = Gio.content_type_guess(file, data=None)

        if not content_type:
            all_apps = []
            default_app = None
        else:
            all_apps = Gio.AppInfo.get_all_for_type(content_type)
            default_app = Gio.AppInfo.get_default_for_type(content_type, False)

        logging.debug(f"_get_apps_for_files {file} {content_type} {len(all_apps)}")

        if all_apps_result is None:
            # first iteration of loop
            all_apps_result = all_apps
            default_app_result = default_app

        else:
            app_ids = [a.get_id() for a in all_apps]
            # usa apenas os comuns
            all_apps_result = [r for r in all_apps_result if r.get_id() in app_ids]

            if not default_app or default_app_result and not default_app_result.equal(default_app):
                # must be the same! or else, set it to None
                default_app_result = None

    if len(all_apps_result) == 0 and all([os.path.isdir(f) for f in files]):
        # special case! if selected file(s) is folder, list of apps will be empty! (content type is "application/octet-stream"??)
        # xdg-open will open the default file manager
        return (Gio.AppInfo.create_from_commandline("xdg-open", "Xdg-open", 0), [])

    return (default_app_result, all_apps_result)

# TODO this should be in external config file
RE_TERMINAL = re.compile("^terminator|x-term|gnome-terminal-server$")
RE_CMDNAME_SHELL = re.compile(".*/bash$")

# TODO should allow user to override this in config.yaml
# example:
# daniel@hello: ~/comp/CtxSearch
# was defined with something like this:
# PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
RE_TITLE = re.compile(r'^\w+@\w+ *: *(.*)$')

class TerminalPlugin(PluginBase):
    dependencies = ["system_info"]

    # E se for uma subshell?
    # ou um ssh? navegar na hierarquia de processos...?

    # 1. Knowing the pid of the process where the selecion was made, detects if it is a terminal.
    # 2. Then find the pid os the shell inside that terminal, and it's current working directory.
    # 3. Uses the directory to detect the files included in the selection.
    # 4. If all the text matches, then it considers a sucess.
    # 5. If it is a multi terminal, can have more than one shell inside. In that case, only proceeds if the match was sucesseful in just one shell.
    def prepare_context(self, ctx):
        if not RE_TERMINAL.match(ctx["program_name"]):
            # Not terminal
            return False

        logging.debug(f"prepare_context title={ctx['window_title']}")

        # Need to know the current directory of the shell running on this terminal
        # Usually, it will be in the window title!

        # IF not this way, I could find all direct bash processes running as childreen of the terminal,and find their cwd
        # For terminator, there can be more than one! Even if I have multiple terminator windows, all will share the same pid...
        # just give up

        m = RE_TITLE.match(ctx['window_title'])
        if not m:
            return

        base_dir = os.path.expanduser(m.group(1))
        logging.debug(f"base_dir={base_dir}")

        try:
            files = lsparse(base_dir, ctx["text"])
        except:
            logging.debug(f"lsparser error {sys.exc_info()[1]}")
            files = None

        logging.debug(f"files={files}")
        ctx["terminal_files"] = files

    # files is list of absolute path, as str
    def _openfile(self, mi, app, files):
        logging.debug(f"_openfile {app}, {files}")

        # list of Gio.File
        giofiles = [Gio.Vfs.get_local().get_file_for_path(file) for file in files ]

        # Could also do this using the app cmdline attribute(?)
        app.launch(giofiles)

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if not "terminal" in action:
            return

        # list of paths
        files = ctx.get("terminal_files")

        if not files:
            return

        add_menuitem_callback(Gtk.SeparatorMenuItem())

        if len(files) == 1:
            # vf = Gio.Vfs.get_local().get_file_for_path(file)
            # content_type, certain = Gio.content_type_guess(file, data=None)
            # logging.debug(f"vf={vf}")
            # logging.debug(content_type) # example: application/octet-stream
            # logging.debug(certain)

            file = files[0]
            filesize = os.path.getsize(file)
            label = f"{path_replace_user(file)}\n{filesize} bytes"

        else:
            dir = folders_common_ancestor(files)
            label = f"{len(files)} files in {path_replace_user(dir)}"

        label_mi = Gtk.MenuItem(label=label)
        # dont want to make set_sensitive False... don't like the look and feel. If user selects this menuitem, nothing will happen
        add_menuitem_callback(label_mi)

        # If it is only one file, preview it
        if len(files) == 1:
            img = preview_image(files[0])
            if img:
                hbox = Gtk.HBox()
                hbox.pack_start(img, expand=False, fill=False, padding=0)

                vbox = Gtk.VBox()
                #vbox.add(Gtk.Label(label=label))
                vbox.add(hbox)

                mi = Gtk.ImageMenuItem()
                mi.add(vbox)
                mi.show_all()
                add_menuitem_callback(mi)

        default_app, all_apps = get_apps_for_files(files)

        if not default_app and len(all_apps) == 0:
            mi = Gtk.MenuItem(label="No application to open file(s)")
            mi.show()
            mi.set_sensitive(False)
            add_menuitem_callback(mi)

        else:
            if default_app:
                mi = Gtk.MenuItem(label="Open with " + default_app.get_display_name())
                mi.show_all()
                mi.connect("activate", self._openfile, default_app, files)
                add_menuitem_callback(mi)

            # for open with extra options
            openwith_submenu = Gtk.Menu()

            for app in all_apps:
                # dont show twice the default app
                if not (default_app and app.equal(default_app)):
                    ##logging.debug(f"x={app.get_display_name()},{app.get_commandline()}")
                    mi = Gtk.MenuItem(label=""+app.get_display_name())
                    mi.show_all()
                    mi.connect("activate", self._openfile, app, files)
                    openwith_submenu.append(mi)

            if len(openwith_submenu.get_children()) > 0:
                # show the submenu with more options
                mi = Gtk.MenuItem(label="Open with...")
                mi.set_submenu(openwith_submenu)
                add_menuitem_callback(mi)

        # CLIPBOARD operations

        mi = Gtk.MenuItem(label="Copy")
        mi.show_all()
        mi.connect("activate", lambda mi: file_ops.clipboard_copy(files))
        add_menuitem_callback(mi)

        mi = Gtk.MenuItem(label="Cut")
        mi.show_all()
        mi.connect("activate", lambda mi: file_ops.clipboard_cut(files))
        add_menuitem_callback(mi)

        # only show the PASTE option if selected file is folder and clipboard has files
        if len(files)==1 and os.path.isdir(files[0]):
            def has_files_cb(yes):
                logging.debug(f"has_files_cb {yes}")
                if yes:
                    mi = Gtk.MenuItem(label="Paste into folder")
                    mi.show_all()
                    mi.connect("activate", lambda mi: file_ops.clipboard_paste(files[0]))
                    add_menuitem_callback(mi)
            file_ops.clipboard_has_files(has_files_cb)

        # DRAG AND DROP
        mi = Gtk.MenuItem(label="Drag")
        mi.show_all()
        mi.connect("button-release-event", lambda mi, ev: file_ops.begin_drag_files(mi, files))
        add_menuitem_callback(mi)

        # OTHER FILE OPS
        mi = Gtk.MenuItem(label="Delete")
        mi.show_all()
        mi.connect("activate", lambda mi: file_ops.delete_confirm(files))
        add_menuitem_callback(mi)

        add_menuitem_callback(Gtk.SeparatorMenuItem())

if __name__ == "__main__" :
    # TESTar assim:
    # ./test.sh plugin_terminal.plugin_terminal
    from optparse import OptionParser
    parser = OptionParser()
    (options, args) = parser.parse_args()

    print(get_apps_for_files(args))
