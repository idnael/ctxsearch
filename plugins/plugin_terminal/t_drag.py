import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

import os, sys, time

def begin_drag(w):
    print("begin_drag")

    def drag_begin_cb(widget, drag_context, data=None):
        print("drag_begin_cb")

    def drag_data_get_cb(widget, drag_context, selection_data, info, time, data=None):
        print("drag_data_get_cb")
        uris = ["file:" + os.path.abspath(path) for path in sys.argv[1:]]
        selection_data.set_uris(uris)

    def drag_data_delete_cb(widget, drag_context, data=None):
        print("drag_data_delete_cb")

    def drag_end_cb(widget, drag_context, data=None):
        print("drag_end_cb")

    w.connect("drag_begin", drag_begin_cb)
    w.connect("drag_data_get", drag_data_get_cb)
    w.connect("drag_data_delete", drag_data_delete_cb)
    w.connect("drag_end", drag_end_cb)

    at = Gdk.Atom.intern("text/uri-list", True)
    # ERRO:
    # #te = Gtk.TargetEntry("text/uri-list", 0, 12340)
    #tl = Gtk.TargetList.new([te])

    tl = Gtk.TargetList()
    tl.add(at, 0, 12340)
    mask = Gdk.ModifierType.BUTTON1_MASK
    w.drag_begin_with_coordinates(tl, Gdk.DragAction.MOVE | Gdk.DragAction.COPY, mask, None, -1, -1)

def on_mi_mouse_press(mi, event):
    print("on_mouse_press")
    # print(event.button)
    # print(event.state)
    # begin_drag(mi, event)
    # menu.hide() # se nao fizer isto, o menu continua visivel...
    # return True

def on_mi_mouse_release(mi, event):
    print("on_mouse_release")
    begin_drag(mi)

def on_mi_activate(mi):
    print("on_activate")

# show the menu
menu = Gtk.Menu()
mi = Gtk.MenuItem(label="drag")
mi.show()
mi.connect("button-press-event", on_mi_mouse_press)
mi.connect("button-release-event", on_mi_mouse_release)
mi.connect("activate", on_mi_activate)
menu.append(mi)
menu.show_all()
menu.popup(None, None, None, None, 1, time.time())

# window = Gtk.Window()
# window.set_size_request(200, 100)
# window.set_title("GTK Menu Test")
# window.connect("delete_event", lambda w, e: Gtk.main_quit())
# button = Gtk.Button(label="press me")
# button.connect("button-press-event", begin_drag)
# button.show()
#window.add(button)
# window.show()

Gtk.main()
