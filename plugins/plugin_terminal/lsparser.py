import os, re

# right pad lines with spaces to make all hthe same len
def align_right(lines):
    maxlen = max(len(line) for line in lines)
    return [line + " " * (maxlen - len(line)) for line in lines]

# lines should have all the same len
# return set with columns that are spaces in all lines
def findspaces(lines):
    line0 = lines[0]
    # print(f"line0 {line0}")
    candi = set([i for i in range(len(line0)) if line0[i] ==' '])
    # print(f"candi={candi}")
    for line in lines[1:]:
        # print(f"line {line}")
        candi2 = set(i for i in candi if line[i] == ' ')
        candi = candi2
        # print(f"candi={candi}")
    return candi

# lines should be all same len
# retruns ordered list with column starts
def find_column_starts(lines):
    spaces = findspaces(lines)
    # em sequencias de espaços, usa apenas o ultimo. O inicio da coluna sera a posicao seguinte
    # nota: iterar em um set nao garante que vem ordenado, embora normalmente aconteça.
    result = [p+1 for p in sorted(list(spaces)) if p+1 not in spaces]
    if 0 not in result:
        # if first column starts at 0, it won't be in spaces, so add it!
        result=[0]+result
    return result

# creates a string with column pos. Useful for logging results
# example:
# ruler2str([2,6,8]) == "  2   6 8"
def ruler2str(tabs):
    linestr = ""
    for tab in tabs:
        if tab > 1000: break
        tabstr = str(tab)
        linestr += " " * (tab - len(linestr)) + tabstr
    return linestr

class _LsParser:
    def __init__(self, basedir, text):
        self.basedir = basedir
        self.text = text
        lines = align_right(text.split("\n"))
        tabs = find_column_starts(lines)

        # add an extra value for the end of last column
        tabs.append(1+len(lines[0]))

        # print(f"BDIR: {basedir}")
        # print(f"TEXT:\n{text}")
        # print(ruler2str(tabs))

        self.files = []

        t0 =0
        while t0 < len(tabs)-1:
            p0 = tabs[t0]
            # now, try to find the end of this column. Might not be the next tab, if files have spaces
            for t1 in range(t0+1, len(tabs)):
                p1 = tabs[t1]
                res = self.column_ok(lines, p0, p1 - 1)
                if type(res) == list:
                    self.files = self.files + res
                    t0 = t1
                    break
            else:
                raise Exception("mrd")

    # converts a name to a absolute path, relative to basedir
    def absfile(self, name):
        if name.startswith("~/") or name == "~":
            return os.path.expanduser(name)
        else:
            return os.path.join(self.basedir, name)

    # verifica no conjunto de linhas, a coluna que começa em p0 e termina em p1-1
    # Se erro, retorna False
    # Se ok, retorna lista de abs paths encontrados na coluna (essa lista pode ser vazia)
    def column_ok(self, lines, p0, p1):
        #print(f"column_files_exist {p0}, {p1}")
        files = []
        for line in lines:
            cell = line[p0:p1]
            cell_result = self.cell_ok(cell)
            if cell_result == False: # should not use "if cell_result" here, that would match also []
                return False
            if cell_result is not None:
                files.append(cell_result)
        return files

    # Verifica uma celula.
    # retorna False se errado
    # None se ok mas nao é um File
    # str path se ok e é um file
    def cell_ok(self, cell):
        if cell.strip() == "":
            return None
        if cell[0] == " ":
            # ignora apenas um espaço para a esquerda
            # LS can add a left space, if other values in ths column have spaces, and are surrounded by "'"
            cell = cell[1:]
        # ignora todos os espaços para a direita
        cell = cell.rstrip()
        f = self.absfile(cell)
        if os.path.exists(f):
            return f
        if cell.startswith("'") and cell.endswith("'"):
            cell = cell[1:-1]
            f = self.absfile(cell)
            return f
        return False

def lsparse(basedir, text):
    lines = text.split("\n")
    maxpad= max(len(line) for line in lines)

    for pad in range(0, maxpad-1):
        try:
            lsp = _LsParser(basedir, (" " * pad) + text)
            print(f"pad={pad}")
            return lsp.files
        except:
            pass
    raise Exception("mrd2")

if __name__ == "__main__" :
    # TESTar assim:
    # ./test.sh plugin_terminal.lsparser2

    BDIR = "/home/daniel"

    # Example of output from "ls" command. In the second line there is a name with space, so ir surrounded with single quotation marks
    # and other values in the column will have SPACE in the left:
    # TEXT = """
    # armazem     Backup             comp         Downloads   fotos               LIXO    abc_ola.txt~   recuperar.txt~   t2.txt~
    # ARQUIVADO  'Calibre Library'   documentos   etc         _ids_remover.txt~   mount   nohup.out      sistema"""[1:] # remove starting newline

    # linha final incompleta. Pode acontecer com um ls q nao tem nr suficiente de files
    # TEXT = """
    # armazem     Backup             comp         Downloads   fotos               LIXO    abc_ola.txt~   recuperar.txt~   t2.txt~
    # ARQUIVADO  'Calibre Library'   documentos   etc         _ids_remover.txt~   """[1:]  # remove starting newline

    # linha inicial incompleta. Pode acontecer com uma selecao continua de um ls, que nao apanhou a coluna inicial completa
    TEXT = """
comp         Downloads   fotos               LIXO    abc_ola.txt~   recuperar.txt~   t2.txt~
    ARQUIVADO  'Calibre Library'   documentos   etc         _ids_remover.txt~   mount   nohup.out      sistema"""[1:] # remove starting newline

    files = lsparse(BDIR, TEXT)
    print(f"{len(files)} files:")
    print(files)



