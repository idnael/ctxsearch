import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib, Gio

import os, logging, subprocess, shutil, urllib

# Asks the user if he wants to delete the files, and if he says Yes, deletes then
def delete_confirm(files):

    names_folders = []
    names_files = [] # files that are not folders!
    for file in files:
        if os.path.isdir(file):
            names_folders.append(os.path.basename(file))
        else:
            names_files.append(os.path.basename(file))

    title = ""
    dialog = Gtk.Dialog(title, flags=Gtk.DialogFlags.MODAL  | Gtk.DialogFlags.DESTROY_WITH_PARENT)
    dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)

    vbox = dialog.get_content_area()
    vbox.set_spacing(10)

    if names_folders:
        vbox.add(Gtk.Label("Recursive delete this folders?"))

        # Display the list of files inside a ScrolledWindow because they could be many!
        text = Gtk.TextView()
        text.set_editable(False)
        text.get_buffer().set_text("\n".join(names_folders))

        scroll = Gtk.ScrolledWindow()
        scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scroll.add(text)

        vbox.pack_start(scroll, True, True, 0)

    if names_files:
        if names_folders:
            vbox.add(Gtk.Label("And these files?"))
        else:
            vbox.add(Gtk.Label("Delete these files?"))

        text = Gtk.TextView()
        text.set_editable(False)
        text.get_buffer().set_text("\n".join(names_files))

        scroll = Gtk.ScrolledWindow()
        scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
        scroll.add(text)

        vbox.pack_start(scroll, True, True, 0)

    dialog.show_all()

    response = dialog.run()
    dialog.destroy()

    if response == Gtk.ResponseType.OK:
        # delete the files!
        for file in files:
            if os.path.isdir(file):
                shutil.rmtree(file)
            else:
                os.remove(file)

def clipboard_copy(files):
    # call the clip_files utility, part of ctxsearch
    # It is not possible to copy files to clipboard in python
    # "It looks like set_with_data() isn't exposed through introspection probably due to the function taking two C callbacks (not supported by introspection or bindings)."
    # https://stackoverflow.com/questions/25151437/copy-html-to-clipboard-with-pygobject
    subprocess.Popen(["clip_files", "--copy"]+files).wait()

# Similar, but cut files
def clipboard_cut(files):
    subprocess.Popen(["clip_files", "--cut"]+files).wait()

# convert a list of uris to files
# Ignore the uris which are not files
def uris_to_paths(uris):
  paths = []
  for uri in uris:
    if uri.startswith("file:/"):
      uri = uri[5:] # keep the "/"
      while uri.startswith("//"):
        uri = uri[1:]
      paths.append(urllib.parse.unquote(uri))
  return paths

def clipboard_has_files(callback):
    def clipboard_paste_callback(_clipboard, selectiondata):
        try:
            if len(selectiondata.get_uris()) > 0:
                callback(True)
            callback(False)
        except:
            callback(False)
    clip = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
    clip.request_contents(Gdk.Atom.intern("text/uri-list", True), clipboard_paste_callback)

def clipboard_paste(destination_folder):
    clip = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)

    def clipboard_paste_callback(_clipboard, selectiondata):
        # https://lazka.github.io/pgi-docs/#Gtk-3.0/classes/SelectionData.html#Gtk.SelectionData

        # TODO don't know how to distinguish COPY from CUT
        # so, will always copy files
        logging.debug(f"clipboard_paste_callback")
        # print("uris",selectiondata.get_uris())
        # print("text",selectiondata.get_text())
        # print("format",selectiondata.get_format())
        # print("data",selectiondata.get_data())
        # print("data_type",selectiondata.get_data_type())
        # print("selection",selectiondata.get_selection())

        uris = selectiondata.get_uris()
        files = uris_to_paths(uris)
        logging.debug(f"files={files}")

        command = ["cp", "-rvp"] + files +[destination_folder]
        subprocess.Popen(command) # dont wait, leave in background...

    clip.request_contents(Gdk.Atom.intern("text/uri-list", True), clipboard_paste_callback)

def begin_drag_files(w, files):
    print("begin_drag")

    def drag_begin_cb(widget, drag_context, data=None):
        print("drag_begin_cb")

    def drag_data_get_cb(widget, drag_context, selection_data, info, time, data=None):
        print("drag_data_get_cb")
        uris = ["file:" + os.path.abspath(f) for f in files]
        selection_data.set_uris(uris)

    def drag_data_delete_cb(widget, drag_context, data=None):
        print("drag_data_delete_cb")

    def drag_end_cb(widget, drag_context, data=None):
        print("drag_end_cb")

    w.connect("drag_begin", drag_begin_cb)
    w.connect("drag_data_get", drag_data_get_cb)
    w.connect("drag_data_delete", drag_data_delete_cb)
    w.connect("drag_end", drag_end_cb)

    tl = Gtk.TargetList()
    tl.add(Gdk.Atom.intern("text/uri-list", True), 0, 12340)
    mask = Gdk.ModifierType.BUTTON1_MASK

    w.drag_begin_with_coordinates(tl, Gdk.DragAction.MOVE | Gdk.DragAction.COPY, mask, None, -1, -1)
