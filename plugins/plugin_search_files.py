import subprocess, logging, io
from threading import Thread
import codecs, os, sys
import html, re

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

from ctxsearch.plugin_base import PluginBase

class PrepareMenuWorker:
    # a subprocess instance
    process = None

    item_count = 0

    running = False

    def __init__(self, config, ctx, action, search_re, add_menuitem_callback):
        # logging.debug(f"PrepareMenu ctx={ctx}")

        self.config = config
        self.ctx = ctx
        self.action = action
        self.search_re = search_re

        self.add_menuitem_callback = add_menuitem_callback

        self.title = action.get("label", ctx)
        self.base_dir = action.get("base_dir", ctx)

        self.max_main_items = action.get("max", default=0)

        self.submenu = None

        self.mainmenuitem = Gtk.MenuItem()
        # if max ==0, mainmenuitem will have a submenu, and should be enabled.
        self.mainmenuitem.set_sensitive(self.max_main_items == 0)
        self.update_label()

        add_menuitem_callback(self.mainmenuitem)

        Thread(target=self.scan).start()

    def update_label(self):
        label = self.title

        label += f" - {self.item_count} results"
        if self.running:
            label += " (running)"
        self.mainmenuitem.set_label(label)

    def scan(self):
        self.running = True
        self.update_label()

        cmd = self.action.get("files", self.ctx)
        self.process = subprocess.Popen(cmd, shell=True, cwd=self.base_dir, stdout=subprocess.PIPE)

        input = io.TextIOWrapper(self.process.stdout, encoding='utf8')
        while True:
            if not self.running:
                break

            line = input.readline()
            if not line:
                break  # end

            file = os.path.abspath(os.path.join(self.base_dir, line.rstrip()))
            try:
                self.process_file(file)
            except:
                logging.debug(f"When processing {file}: {sys.exc_info()[1]}")

        if self.process:
            exitstatus = self.process.wait()
            self.process = None

        self.running = False
        self.update_label()

    def clicked(self, mi, file, linenr):
        logging.debug("Clicked in " + file)
        editcmd = self.action.get("editor", self.ctx, {"file":file, "linenr":linenr})
        if not editcmd:
            editcmd = self.config.get(["searchfiles", "editor"], {"file":file, "linenr":linenr})

        logging.debug(f"editcmd={editcmd}")
        subprocess.Popen(editcmd, shell=True)

    # should be called on UI thread
    def add_item(self, file, linenr, line_html, same_file):
        self.item_count += 1
        self.update_label()

        relfile = os.path.relpath(file, self.base_dir)
        if not same_file:
            # we use \n. Doesn't accept <br> or <p>
            html = f"<i>{relfile}</i>:\n" + line_html
        else:
            # just show filename once
            html = line_html

        label = Gtk.Label()
        label.set_markup(html)

        # Vou criar um hbox e colocar a label dentro
        # se fizesse directamente mi.add(label) as labels iriam ficar
        # centradas horizontalmente
        hbox = Gtk.HBox()
        hbox.pack_start(label, expand=False, fill=False, padding=0)

        mi = Gtk.MenuItem()
        mi.add(hbox)
        mi.show_all()
        mi.connect('activate', self.clicked, file, linenr)

        logging.debug(f"add_item {file}, linenr={linenr}, count={self.item_count}, max={self.max_main_items}")

        if self.max_main_items <= 0:
            # the main menu item will have a submenu were all items will be placed
            if not self.submenu:
                self.submenu = Gtk.Menu()
                # logging.debug("adding submenu...")
                self.mainmenuitem.set_submenu(self.submenu)

            self.submenu.append(mi)

        elif self.item_count <= self.max_main_items:
            # there is still room in the main menu
            self.add_menuitem_callback(mi)

        else:
            # add to "more" submenu
            if not self.submenu:
                self.submenu = Gtk.Menu()
                mi0 = Gtk.MenuItem(label="More")
                mi0.set_submenu(self.submenu)
                self.add_menuitem_callback(mi0)

            self.submenu.append(mi)

    def process_file(self, file):
        #logging.debug(f"_ process_file {file}")

        with codecs.open(file, "r", "utf-8", errors='replace') as f:
            content = f.read().splitlines(keepends=False)

        # DOC: linenumber starts at 1

        first = True

        linenr = 0
        for linestr in content:
            # logging.debug(f"linestr {linestr}")

            #p = linestr.find(self.search_text)
            m = self.search_re.search(linestr)

            if not self.running:
                break

            linenr +=1

            #if p != -1:
            if m:
                #before = linestr[:p]
                #after = linestr[p + len(self.search_text):]
                before = linestr[:m.start(0)]
                matched = m.group(0)
                after = linestr[m.end(0):]

                line_html = f"{html.escape(before)}<b>{html.escape(matched)}</b>{html.escape(after)}"

                GLib.idle_add(self.add_item, file, linenr, line_html, not first)

                first = False

    def kill(self):
        if self.process:
            self.process.kill()
            self.process = None

        self.running = False


class SearchFilesPlugin(PluginBase):
    def __init__(self, ctxsearch):
        PluginBase.__init__(self, ctxsearch)

    workers = []

    def menu_destroyed(self):
        # logging.debug("menu_destroyed")
        for w in self.workers:
            w.kill()
        self.workers.clear()


    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if not "search_files" in action:
            # not for me
            return

        if "base_dir" not in action or "files" not in action:
            raise Exception("Should have attrs base_dir, files")

        search_text = action.get("search_text", ctx, default=ctx["text"])
        logging.debug(f"search_text={search_text}")

        if search_text == "":
            # maybe was defined in an expression, with match(), that didn't match, so is empty
            return

        # search whole word
        search_re = re.compile('\\b' + re.escape(search_text) + '\\b')

        self.workers.append(PrepareMenuWorker(self.config, ctx, action, search_re, add_menuitem_callback))



