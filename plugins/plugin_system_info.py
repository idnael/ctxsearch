import subprocess, os, psutil, io, logging

from ctxsearch.plugin_base import PluginBase

def get_active_window():
    # TODO could do this with python gtk api???
    process = subprocess.Popen(["xdotool","getactivewindow"], stdout=subprocess.PIPE)
    wid = int(process.stdout.readline().strip())
    return wid

def get_window_pid_and_title(wid):
    process = subprocess.Popen(["xdotool", "getwindowname", str(wid), "getwindowpid", str(wid)], stdout=subprocess.PIPE)

    stdout = io.TextIOWrapper(process.stdout, encoding='utf8')

    # window title?
    title = stdout.readline().strip()
    pid = int(stdout.readline().strip())
    return pid,title

def get_program_name(pid):
    proc = psutil.Process(pid)
    name = proc.name()

    i = name.find(" ")
    if i != -1: name = name[0 : i]

    i = name.rfind("/")
    if i != -1: name = name[i+1 :]

    return name

# adds this attributes to the context:
# wid: active window id
# pid: process id of active window
# program_name: from pid
class SystemInfoPlugin(PluginBase):
    ##dependencies = ["languagex"]

    # def init(self, ctxsearch):
    #     PluginBase.init(self, ctxsearch)

    def prepare_context(self, ctx):
        # TODO e o x y (e time) ????

        # See which window is active now, which should be where the clipboard selection was made
        # TODO xwindows.get_active_window
        ctx["wid"] = get_active_window()

        # get the program name
        try:
            ctx["pid"], ctx["window_title"] = get_window_pid_and_title(ctx["wid"])

            logging.debug(f"_window_title={ctx['window_title']}")

        except Exception as ex:
            logging.exception(f"Error in get_window_pid_and_title: {ex}")
            ctx["pid"] = None
            ctx["window_title"] = None

        ctx["program_name"] = get_program_name(ctx["pid"])

        logging.debug(f"pid={ctx['pid']}")
        logging.debug(f"program_name={ctx['program_name']}")
