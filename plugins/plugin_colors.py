import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

import re
from PIL import Image, ImageDraw, ImageFont

from ctxsearch.plugin_base import PluginBase

# If selected text is a color, shows a box painted with that color

COLOR_RE = re.compile("#?\\b([a-fA-F0-9]{6})\\b")
# exemplos: ff0000  0FF000

# Uso as funcoes deprecated override_background_color e box.override_color
# A forma correcta seria usar CSS...
# criar uma image - PIL??? # numpy - opencv??? - como usei no plates...
# VER
# https://pillow.readthedocs.io/en/stable/handbook/tutorial.html#using-the-image-class
# https://pillow.readthedocs.io/en/stable/handbook/tutorial.html

WIDTH = 200
HEIGHT = 100
class ColorsPlugin(PluginBase):

    def prepare_menu(self, ctx, action, add_menuitem_callback):
        if "colors" not in action:
            return

        m = COLOR_RE.match(ctx["text"])
        if not m:
            return

        rgb_str = m.group(1)

        color = Gdk.RGBA()
        color.parse("#"+rgb_str.lower())

        contrast = Gdk.RGBA(1-color.red, 1-color.green, 1-color.blue, 1)
        lab = Gtk.Label("\n"+rgb_str+"\n")

        box = Gtk.HBox()
        box.pack_start(lab, expand=True, fill=True, padding=20)

        box.override_background_color(Gtk.StateFlags.NORMAL, color)
        box.override_color(Gtk.StateFlags.NORMAL, contrast)

        mi = Gtk.ImageMenuItem()
        mi.add(box)

        # faz com que nao seja selecionavel
        # problema: ao fazer isso, o sistema muda a cor do texto para ficar mais esbatido, e perco o contraste...
        # mi.set_sensitive(False)

        mi.show_all()


        add_menuitem_callback(mi)
