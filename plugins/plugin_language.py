import locale

import logging

import langdetect

from ctxsearch.plugin_base import PluginBase

### DEFAULT_LANGID_CONFIDENCY = 0.999  ## TODO config!

class LanguagePlugin(PluginBase):

    # def init(self, ctxsearch):
    #     PluginBase.init(self, ctxsearch)

    def prepare_context(self, ctx):
        # TODO? if there is no support, put ""
        logging.debug("prepare_context")

        accepted_langs = {}

        # example: if config has "pt(es),en"
        # means if we find "es" (spanish), use "pt" (portuguese) instead
        for part in self.config.get("languages", default="en").split(","):
            p = part.find("(")
            if p == -1:
                lang = part.strip()
                accepted_langs[lang] = lang
            else:
                p2 = part.find(")",p)
                lang = part[0:p].strip()
                accepted_langs[lang] = lang
                for other in part[p+1:p2].split(","):
                    accepted_langs[other.strip()] = lang

        try:
            results = langdetect.detect_langs(ctx["text"])

            if len(results) == 0:
                return

            # apenas as langs configuradas
            results = [result for result in results if result.lang in accepted_langs]

            # a maior prob
            best = max(results, key=lambda result: result.prob)

            best_lang = accepted_langs[best.lang]

            logging.debug(f"detected lang={best_lang}")

            ctx["language"] = best_lang

        except:
            # da erro se nao tiver nada...
            ctx["language"] = ""

    def add_default_config(self, config):
        if "languages" not in config.data:
            logging.debug("add_default_config - adding lang")
            # getdefaultlocale da por exemplo ('en_US', 'UTF-8')
            # eu quero apenas o "en"
            lang = locale.getdefaultlocale()[0][0:2]
            config.data["languages"] = ",".join(list(set([lang, "en"])))

            # add a comment after the new item! using method from ruamel.yaml!
            # see https://yaml.readthedocs.io/en/latest/detail.html
            config.data.yaml_add_eol_comment('languages to be detected', 'languages')

            return True

if __name__ == "__main__" :
    # TESTar assim:
    # ./test.sh plugin_language "texto"

    import sys, time

    # TESTE DIRECTO DO LANGDETECT
    for text in sys.argv[1:]:
        t0=time.time()
        results = langdetect.detect_langs(text)

        delta= time.time() - t0
        print(f"Results for {text}:")
        print(results)
        print(f"{delta} seconds")


    # # TESTE PLUGIN
    #
    # from ctxsearch.fake import *
    #
    # config = FakeConfig({"languages":"pt(es),en"})
    # ctxsearch = FakeContextSearch(config)
    #
    # lp = LanguagePlugin(ctxsearch)
    #
    # for text in sys.argv[1:]:
    #     ctx = {"text":text}
    #     lp.prepare_context(ctx)
    #     print(ctx)
