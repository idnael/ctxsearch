
import logging, subprocess, os

from ctxsearch.plugin_base import PluginBase
from ctxsearch.globals import DATA_DIR, STATE_DIR

#from .webwindow import WebWindow ERRO
# from webwindow import WebWindow ERRO

from .webwindow import WebWindow

# open url in internal webview window
class WebviewPlugin(PluginBase):
    dependencies = ["url"]

    def __init__(self, ctxsearch):
        PluginBase.__init__(self, ctxsearch)
        # logging.debug("WebviewPlugin constructor")
        self.webwindow = None

    def does_open_url(self):
        return True

    def open_url(self, url, ctx, action):
        if not self.webwindow:
            self.webwindow = WebWindow(
                STATE_DIR,
                on_about=self.about,
                on_edit_config=self.edit_config)

        webview_cfg = self.config.get("webview", default={})

        external_plugin_name = self.config.get(["url", "external"], default=None)

        if external_plugin_name:
            external_plugin = self.ctxsearch.pluginman.by_name(external_plugin_name)
            open_external = lambda newurl: external_plugin.open_url(newurl, ctx, action)

        else:
            open_external = None

        self.webwindow.open_url(webview_cfg, url, ctx, action, open_external=open_external)

    def edit_config(self, menuitem):
        subprocess.Popen(["xdg-open", self.ctxsearch.config.cfg_file])

    def about(self, menuitem):
        # TODO abrir pagina?
        from ctxsearch.aboutwindow import AboutWindow
        AboutWindow()
