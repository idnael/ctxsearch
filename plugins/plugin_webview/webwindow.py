# -*- coding: utf-8 -*-

import pipes, os, time, subprocess, logging

import gi

# 20190530 no freixo tem que ser 4.0 (freixo)
# no redol tem que ser 3.0...
# TODO rever isto
try:
    gi.require_version('WebKit2', '3.0')
    logging.debug("Using webkit 3.0")
except ValueError:
    gi.require_version('WebKit2', '4.0')
    logging.debug("Using webkit 4.0")

gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk,GObject, WebKit2, GLib
import cairo

# TODO remove this import after changing loadingwidget to not use image?
from ctxsearch.globals import DATA_DIR
from ctxsearch.actions import Action

DEFAULT_WIDTH = 600
DEFAULT_HEIGHT = 400

KEY_ESCAPE = 9
KEY_F = 41

class FindBox(Gtk.HBox):
    MAX_COUNT = 100

    def __init__(self, findctrl):
        Gtk.HBox.__init__(self)

        self.findctrl = findctrl
        self.findctrl.connect("counted-matches", self.on_counted_matches)
        self.findctrl.connect("found-text", self.on_search_sucess)
        self.findctrl.connect("failed-to-find-text", self.on_search_failed)

        self.entry = Gtk.Entry()
        self.btn_prev = Gtk.Button(label="<<")
        self.btn_next = Gtk.Button(label=">>")
        self.btn_hide = Gtk.Button(label="X")
        self.lab_count = Gtk.Label()

        self.lab_count.set_margin_left(30)
        self.lab_count.set_margin_right(30)

        self.btn_next.connect("clicked", self.on_clicked_next)
        self.btn_prev.connect("clicked", self.on_clicked_prev)
        self.btn_hide.connect("clicked", self.on_clicked_hide)

        self.entry.connect("key-press-event", self.on_key_press)
        self.entry.connect("changed", self.on_text_changed)

        self.pack_start(self.entry, True, True, 0)
        self.pack_start(self.btn_prev, False, False, 0)
        self.pack_start(self.btn_next, False, False, 0)
        self.pack_start(self.btn_hide, False, False, 0)
        self.pack_start(self.lab_count, False, False, 0)

        self.show_all()

        self.ready = False

    def on_clicked_next(self, *args):
        logging.debug("clicked_next")
        if not self.ready:
            txt = self.entry.get_text()
            self.findctrl.count_matches(txt, WebKit2.FindOptions.CASE_INSENSITIVE, self.MAX_COUNT)
            self.findctrl.search(txt, WebKit2.FindOptions.CASE_INSENSITIVE, self.MAX_COUNT)
            self.ready = True
        else:
            self.findctrl.search_next()

    def on_clicked_prev(self, *args):
        logging.debug("clicked_prev")
        if not self.ready:
            txt = self.entry.get_text()
            self.findctrl.count_matches(txt, WebKit2.FindOptions.CASE_INSENSITIVE, self.MAX_COUNT)
            self.findctrl.search(txt, WebKit2.FindOptions.CASE_INSENSITIVE | WebKit2.FindOptions.BACKWARDS, self.MAX_COUNT)
            self.ready = True
        else:
            self.findctrl.search_previous()

    def on_key_press(self, webview, event, data=None):
        # logging.debug(f"findbox on_key_press {event} {event.hardware_keycode}")
        if event.hardware_keycode == KEY_F and event.state & Gdk.ModifierType.CONTROL_MASK:
            self.on_clicked_next()

    def open(self):
        logging.debug("open")
        self.show()
        # must be realized...?
        self.entry.grab_focus()
        #GLib.timeout_add(100, lambda: self.entry.grab_focus())

    def on_clicked_hide(self, *args):
        logging.debug("clicked_hide")
        if self.entry.get_text():
            self.entry.set_text("")
            self.entry.grab_focus()


        else:
            self.hide()

    def on_text_changed(self, ed):
        txt = ed.get_text()
        logging.debug(f"on_text_changed {txt}")
        # for the counting to appear
        self.findctrl.count_matches(txt, WebKit2.FindOptions.CASE_INSENSITIVE, self.MAX_COUNT)
        # show first match
        self.findctrl.search(txt, WebKit2.FindOptions.CASE_INSENSITIVE, self.MAX_COUNT)
        self.ready = True

    def on_counted_matches(self, fc, count):
        logging.debug(f"on_counted_matches {count}")
        self.lab_count.set_text(f"{count} matches")

    def on_search_sucess(self, fc):
        logging.debug("on_search_sucess")

    def on_search_failed(self, fc):
        logging.debug("on_search_failed")

    # call this when page changed in webvie
    def reset_page(self):
        logging.debug("reset_page")
        self.ready = False

# Uma janela com um webview. É preservada entre chamadas. Quando nao é usada, esconde-se. Assim é mais rapido (?)
# the cfg is not a ctxsearch.Config, it is only part of it, with props width etc.
# open_external is a function that receives an url an opens it in external browser, or None if functionality not available
class WebWindow(Gtk.Window):
    ctx = None
    action = None

    def __init__(self, state_dir, on_about=None, on_edit_config=None):
        Gtk.Window.__init__(self)
        self.on_about = on_about
        self.on_edit_config = on_edit_config

        # https://stackoverflow.com/questions/48368219/webkit2-webview-how-to-store-cookies-and-reuse-it-again
        context = WebKit2.WebContext.get_default() # podia fazer isto apartir do DataManager?
        self.cookies = context.get_cookie_manager()
        ucmanager = WebKit2.UserContentManager() ##NEEDED ???
        self.webview = WebKit2.WebView.new_with_user_content_manager(ucmanager)

        self.webview.get_website_data_manager().set_tls_errors_policy(WebKit2.TLSErrorsPolicy.IGNORE)

        self.cookies.set_accept_policy(WebKit2.CookieAcceptPolicy.ALWAYS)
        self.cookies.set_persistent_storage(os.path.join(state_dir, "cookies.txt"), WebKit2.CookiePersistentStorage.TEXT)

        dm = self.webview.get_website_data_manager()
        print("dm=",dm.get_disk_cache_directory()) ## /home/daniel/.cache/webwindow.py -- Mas parece q nao faz cache pq é lento...
        #settings = self.webview.get_settings()
        #settings.set_enable_page_cache(True)

        # uses the same icon as the floating window
        # TODO self.set_icon_from_file(os.path.join(self.cfg.data_dir,"app_icon.png"))

        # find box!
        self.findbox = FindBox(self.webview.get_find_controller())
        self.findbox.hide()

        self.topbox = Gtk.VBox()
        self.topbox.pack_start(self.findbox, False, True, 0)
        self.topbox.pack_start(self.webview, True, True, 0)
        self.topbox.show()

        self.opening_url = False

        self.webview.show()

        self.add(self.topbox)

        self.first_open = True

        # About context menus... https://stackoverflow.com/questions/55910976/how-to-modify-webkit-context-menu
        self.webview.connect("context-menu", self.context_menu)
        self.webview.connect("context-menu-dismissed", lambda w: logging.debug("context-menu-dismissed"))

        self.connect("delete-event", self.on_window_delete)

        self.webview.connect("load_changed", self.webview_load_changed_cb)

        # TODO need to add to the  webview... and self also, for when webview is not created yet??
        self.connect("key-press-event", self.webpage_key_press)
        self.webview.connect("key-press-event", self.webpage_key_press)

        # temporario...?
        self.loading_image = cairo.ImageSurface.create_from_png(os.path.join(DATA_DIR, "plugins/webview/loading.png"))

    def do_draw(self, context):
        # logging.debug("do_draw")

        # Call super method:
        Gtk.Window.do_draw(self, context)

        if self.opening_url:
            # shows a square on top of the webview, with the message "Loading"
            # TODO couldn't find a way to do this with a simple view that could be show/hide
            # so had to override do_draw

            # context.move_to(0,0 )
            # context.line_to(400,400)
            # context.stroke()

            win_w, win_h = self.get_size()

            img_w = self.loading_image.get_width()
            img_h = self.loading_image.get_height()

            scale = min(win_w/2/img_w, win_h/2/img_h)

            context.translate((win_w-img_w*scale)/2, (win_h-img_h*scale)/2)
            context.scale(scale, scale)

            context.set_source_surface(self.loading_image, 0, 0)
            context.paint()

            # nada aparece
            # context.move_to(0,0 )
            # context.show_text("LOADING!")
            # context.stroke()

    def on_window_delete(self, win, event):
        self.first_open = True

        # just hide the window:
        self.hide()

        # this will prevent the default action, which is to destroy the window
        return True

    def webpage_key_press(self, webview, event, data=None):
        # logging.debug(f"webpage_key_press {event} {event.hardware_keycode}")
        #print(self.webview.get_favicon()) Retorna SEMPRE NONE

        # 201905 como nao consegui colocar a funcionar o contetxt menu, defini que shift + ESC fecha o webview e abre o url no default browser

        if event.hardware_keycode == KEY_ESCAPE:
            # ESCAPE key closes the window!
            if self.findbox.is_visible():
                self.findbox.hide()

            elif self.opening_url:
                logging.debug("cancelling load")
                self.webview.stop_loading()
                self.opening_url = False
                self.queue_draw()

            else:
                if event.state & Gdk.ModifierType.SHIFT_MASK:
                    logging.debug("SHIFT")
                    self.menu_open_external(None, self.webview.get_property("uri"), True)
                self.hide()

        elif event.hardware_keycode == KEY_F and event.state & Gdk.ModifierType.CONTROL_MASK:
            logging.debug("NEXT!!!")
            self.findbox.open()

    # This will be called when the WebView context menu is about to be displayed
    # arguments:
    # context_menu is a WebKit2.ContextMenu
    # hit_test_result is a WebKit2.HitTestResult
    def context_menu(self, web_view, context_menu, event, hit_test_result):
        # context_menu is a WebKit2.ContextMenu
        logging.debug(f"context_menu {context_menu}, {event}, {hit_test_result}")

        ACTIONS_TO_IGNORE = [WebKit2.ContextMenuAction.DOWNLOAD_LINK_TO_DISK,
                             WebKit2.ContextMenuAction.DOWNLOAD_VIDEO_TO_DISK,
                             WebKit2.ContextMenuAction.DOWNLOAD_AUDIO_TO_DISK,
                             WebKit2.ContextMenuAction.DOWNLOAD_IMAGE_TO_DISK,
                             WebKit2.ContextMenuAction.OPEN_LINK_IN_NEW_WINDOW,
                             WebKit2.ContextMenuAction.OPEN_VIDEO_IN_NEW_WINDOW,
                             WebKit2.ContextMenuAction.OPEN_AUDIO_IN_NEW_WINDOW,
                             WebKit2.ContextMenuAction.OPEN_IMAGE_IN_NEW_WINDOW,
                             WebKit2.ContextMenuAction.OPEN_FRAME_IN_NEW_WINDOW,
                             ]

        for item in context_menu.get_items():
            if item.get_stock_action() in ACTIONS_TO_IGNORE:
                context_menu.remove(item)

        clicked_link = hit_test_result.get_link_uri()

        # add a separator before our options
        context_menu.append(WebKit2.ContextMenuItem.new_separator())

        action = Gtk.Action(name="ctxsearch_find", label="Find",
                            tooltip="Find text", stock_id=Gtk.STOCK_NEW)
        action.connect("activate", lambda a:self.findbox.open())
        option = WebKit2.ContextMenuItem().new(action)
        context_menu.append(option)

        if not clicked_link:
            # nota: o webview ja cria o contextmenu para copiar o link

            # o WebKit2.ContextMenuItem diz que agora tenho que usar Gio.Action mas ainda nao sei usar isso
            action = Gtk.Action(name="ctxsearch_copy_url", label="Copy page url", stock_id=Gtk.STOCK_NEW)
            action.connect("activate", self.menu_copy_url, self.webview.get_property("uri"))
            option = WebKit2.ContextMenuItem().new(action)
            context_menu.append(option)

        if self.open_external:
            if clicked_link:
                action = Gtk.Action(name="ctxsearch_browser", label="Open link in browser", stock_id=Gtk.STOCK_NEW)
                # don't hide webwindow!
                action.connect("activate", self.menu_open_external, clicked_link, False)
                option = WebKit2.ContextMenuItem().new(action)
                context_menu.append(option)

            else:
                action = Gtk.Action(name="ctxsearch_browser", label="Open in browser", stock_id=Gtk.STOCK_NEW)

                # the current url, might be different from the one passed in open_url
                # hides the webwindow
                action.connect("activate", self.menu_open_external, self.webview.get_property("uri"), True)
                option = WebKit2.ContextMenuItem().new(action)
                context_menu.append(option)

        if self.on_about:
            action = Gtk.Action(name="ctxsearch_about", label="About CtxSearch", tooltip=None, stock_id=Gtk.STOCK_NEW)
            action.connect("activate", self.on_about)
            option = WebKit2.ContextMenuItem().new(action)
            context_menu.append(option)

        if self.on_edit_config:
            action = Gtk.Action(name="ctxsearch_edit", label="Edit CtxSearch config", tooltip="Edit configuration", stock_id=Gtk.STOCK_NEW)
            action.connect("activate", self.on_edit_config)
            option = WebKit2.ContextMenuItem().new(action)
            context_menu.append(option)

    # ctx can be None
    # action is a Action object, or None
    # open_external is the function to be called to open a url externally, has just one arg, the url
    def open_url(self, webcfg, url, ctx, action, open_external=None):
        logging.debug(f"open_url {url}, first_open={self.first_open}")
        self.ctx = ctx
        self.action = action
        self.webcfg = webcfg
        self.open_external = open_external

        w0, h0 = self.get_size()
        print("previous wh",w0,h0)

        x0, y0 = self.get_position()

        # the window size could be specified in the action
        # on the first time the window opens, it can b e specified in cfg
        # action can specify precise width,height or min values
        if self.first_open:
            w = webcfg.get("width", DEFAULT_WIDTH)
        else:
            w = w0

        if "width" in action:
            w = action["width"]
        elif "minwidth" in action:
            w = max(w, action["minwidth"])

        if self.first_open:
            h = webcfg.get("height", DEFAULT_HEIGHT)
        else:
            h = h0

        if "height" in action:
            h = action["height"]
        elif "minheight" in action:
            h = max(h, action["minheight"])

        if w != w0 or h != h0:
            print("wh end",w,h)
            self.resize(w, h)

        # TODO 201905 agora uso WebView2! talvez tenha funcioanlidades que podem ser uteis!!!

        # clears the previous page while loading the new one
        ###self.webview.load_string("ola", "text/html", "utf-8", "about:blank")
        ###self.webview.clear()

        # problemas com isto?
        #self.webview.execute_script("document.open()")


        # self.webview.connect("notify::favicon", self.favicon) NUNCA é chamado

        # Webwindow is activated, if there was already a page being displayed, it will continue to be displayed
        # after load_uri is called, until the load changed state is COMMITTED.
        # This have a ugly effect. So, we show a loading message on top of the webview
        # while the page is been loaded
        self.opening_url = True
        self.queue_draw() # force do_draw to be called

        self.webview.set_sensitive(False)
        self.webview.load_uri(url)

        if self.first_open and ctx and "x" in ctx and "y" in ctx:
            # show the window where the mouse was when the text selection was made
            x = ctx["x"]
            y = ctx["y"]
        else:
            # keep on same place
            # but need to call self.move or else window would be placed in a default pos, because I called hide() and show()
            x = x0
            y = y0

        # TODO
        # screen_w = self.get_screen().get_width()
        # screen_h = self.get_screen().get_height()
        #
        # #guaranted that the window is inside the screen:
        # if x + w > screen_w:
        #     x = screen_w - w
        # if y + h > screen_h:
        #     y = screen_h - h
        self.move(x, y)

        # if I hide and then show the window, it will be in the current desktop...!
        self.hide()
        self.show()

        # raise the window, if it was covered
        self.present()

        self.first_open = False

    def favicon(self, a,b):
        logging.debug("FAVICON %s %s" % (a,b))

    def menu_copy_url(self, _, url):
        logging.debug(f"menu_copy_url url={url}")

        clip = Gtk.Clipboard.get(Gdk.SELECTION_CLIPBOARD)
        clip.set_text(url, -1)

    def menu_open_external(self, _, url, hide_self=False):
        if not self.open_external:
            return

        logging.debug(f"menu_open_external url={url}")

        if hide_self:
            self.hide()
            
        self.open_external(url)

    # this will be called whenever load state changed. After calling open_url() or when user clicks a link
    def webview_load_changed_cb(self, w, load_event):
        logging.debug(f"webview_load_changed_cb {load_event}")
        self.findbox.reset_page()

        if load_event == WebKit2.LoadEvent.COMMITTED:
            # As soon as the page start loading, meaning the previous page was removed from the view, we can make it sensitive again, allowing the user to scroll or do other actions
            # Meaning of COMMITTED, from the docs: "The content started arriving for a page load. The necessary transport requirements are established, and the load is being performed."

            self.set_title("CtxSearch - " + (self.webview.props.title or ""))

            self.webview.set_sensitive(True)
            self.opening_url = False
            self.queue_draw() #

            js = self.action.get("javascript", self.ctx)
            if js is not None:
                logging.debug(f"js: {js}")
                self.webview.run_javascript(js, None, lambda w,task: logging.debug("js done"))

    # def on_help(self, menuitem):
    #
    #     # TODO: podia ter uma versao html da ajuda!
    #
    #     import urlparse, urllib
    #
    #     url = urlparse.urljoin('file:', urllib.pathname2url(os.path.abspath(self.cfg.help_file)))
    #     self.webview.open(url)


if __name__ == "__main__" :
    # correr assim:
    # $ ./test.sh plugin_webview.webwindow URL

    import signal, os, sys
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    from ctxsearch.etc.mylog import log_to_console
    log_to_console()

    # console = logging.StreamHandler()
    # console.setFormatter(logging.Formatter('%(module)s %(levelname)-8s %(message)s'))
    # logging.getLogger().addHandler(console)
    # logging.getLogger().setLevel(logging.DEBUG)

    logging.debug("teste 123")

    URL = sys.argv[1]

    # URL = "http://gtk.org"
    # URL = "http://www.publico.pt"

    cfg = {"width":600, "height":400}
    w = WebWindow(state_dir="/tmp")

    def open_url():
        print(f"open_url {URL}")
        ctx={"x":500, "y":500}
        action = Action({})
        w.open_url(cfg, URL, ctx, action)

    OPEN_DELAY =2000
    print(f"pid={os.getpid()}")
    print(f"open_url in {OPEN_DELAY} ms")
    GLib.timeout_add(OPEN_DELAY, open_url)

    Gtk.main()
