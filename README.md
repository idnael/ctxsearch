# CtxSearch - Run actions on selected text

Most desktop applications - text processor, web browser, terminal - 
allow the user to selected text, and do actions on the text, using a context menu.

I wanted to add my own personalized actions to these menus. 
Unfortunately, there is no standard way to this.

This is a solution to this problem. 
Run CtxSearch in the background. Selection a region of text in your favorite application.
This will wake up CtxSearch, which will show a floating icon near the mouse position. 

![](20211213_screenshot_floating.png)

Click on it to show a menu of actions you can perform with the selected text.
If you don't want to do any action, just ignore the icon, it will disappear after some moments
without intefering with your work.

![](20211213_screenshot_menu.png)

Examples of actions are: search the text on wikipedia or google, speak it (with a speech synthesizer like emacspeak) etc.

You can customize your actions in _actions.yaml_ configuration file.

<pre>
actions:
   # search any text in Google
   - label: Google search
     url:  https://www.google.pt/search?q=${text.urlquote}

   # search portuguese words in wiktionary
   # uses internal webview instead of default browser:
   - label: Wikcionario PT
     if: text.wordcount==1 and language=="pt"
     url:  http://pt.wiktionary.org/wiki/${text.urlquote}

   # speak english text using an external command
   - label: Espeak en
     command: espeak -v en-us
     command_input: true
     if: language=="en"
</pre>

Predefined functionalities include:
- actions can be filtered based on text language, text size and other text features, name of the active program etc.
- actions can open urls in default browser or in the internal webview, or can execute external commands.

New functionalities can be added by creating python plugins.

## History

It was inspired by a Google Chrome add-on I once used, 
called "Context Search". But I wanted to make a tool that works in any desktop application, not only in the web browser.

I started this project around 2014. The source here is a complete rewrite, to be more customizable and easy to use.


## Compatibility

Only works in Linux systems. Was mainly tested in Linux Mint + Cinnamon. 

Also tested in Ubuntu Unity, KDE, Gnome.

Should work in other environments. Please send feedback if you have any problems.

## How does it work

- When user selects a piece of text, the system copies the text to the primary selection. 
- We receive a clipboard event at this moment so we know the selection was made.
- From then on, we monitor keyboard and mouse events to know if the user is not interested, or we
should just hide the floating icon.
- If the user clicks the icon, we create a menu whose content is dynamically created by the plugins, 
  based on the context and the actions defined in the config file.

## Install from source

Dependencies:
<pre>
sudo apt-get install python3 python3-ruamel.yaml python3-gi python3-gi-cairo xdotool python3-psutil python3-pyqt5.qtwebkit python3-bs4 python3-langdetect python3-html2text imagemagick-6.q16
</pre>

There is non standard _install.sh_ script in the root folder:
<pre>
sudo ./install.sh --install 
</pre>

I'm looking for help to create a source package, which I could use to create a ubuntu repository.

## Install from binaries

You can install the prebuild .deb file which is in the _deb folder

# Some pre-included plugins

## Terminal

Allows you to select one or more files in a terminal window, like in the output of a _ls_ or _find_ program,
and do actions on the files, like open it, copy to clipboard, drag etc. 

It will work only if the paths are relative to the terminal current working directory.

Example:
![](20220409_screenshot_terminal.png)

## Searchfiles

Allows to search the selected text in a range of text files.
It shows a menu entry for each occurrence found.

Config example:
<pre>
actions:
- search_files:
  if: text.match("[0-9]+")  # only uses this action if selected text is a number
  label: Lego inventory
  base_dir: /home/dan/my_inventory
  files: find -name '*.txt'
  edit_file: xed ${file} +${linenr} # tells how to open a file
</pre>

## Others

Select a mathematical expression and see the result

Example - Test here: 1+2+3

![](20220409_screenshot_expr.png)


For LEGO® enthusiasts, I also made this ctxsearch plugin to search part numbers and set numbers in Bricklink.com®: 
https://gitlab.com/idnael/ctxsearch-bricklink
