import re
import lrparsing

from .expr_parser import eval_expression, METHODS

# -----------------------------------------------------------------------
# Defines some high level functions to deal with expressions
# -----------------------------------------------------------------------

# try match
# If groupnr is given returns the given match group, or else, returns all the matched region
# se nao fizer match, retorna None
def _match_function(string, regex, groupnr=0, *flags):
    m = re.search(regex, string, *flags)
    if not m:
        return None
    return m.group(groupnr)

_WORD_SEPARATOR_REGEXP = re.compile(r'[ ,.;!?]+')

# charclass is like "0-9", "a-zA-Z" etc
# returns number between 0 and 1
def count_chars_class(s, charclass):
    r = re.compile("[" + charclass + "]")
    return len(r.findall(s)) / len(s)

# Some default methods:
METHODS[(str, "match")] = _match_function
METHODS[(str, "imatch")] = lambda s, r, groupnr=0: _match_function(s, r, groupnr, re.IGNORECASE)
METHODS[(str, "len")] = len
METHODS[(str, "wordcount")] = lambda s: len(_WORD_SEPARATOR_REGEXP.split(s.strip()))
METHODS[(str, "charcount")] = count_chars_class
METHODS[(str, "lettercount")] = lambda s: count_chars_class(s, "a-zA-Z")

# REplaces expressions in a string
# Example:
# replace_vars("echo ${text} xxx ${text.len}", {"text":"ola mundo"}) ==> "echo ola mundo xxx 9"
# If string contains expression with error, this method will raise exception
def replace_vars(string, vars):
    # logging.debug(f"_replace_vars {string}")
    p = 0
    result = ""
    while True:
        p2 = string.find("${", p)
        if p2 == -1:
            break

        p3 = string.find("}", p2)
        while p3 != -1:
            # the region inside the ${...}
            valuestr = string[p2 + 2: p3]

            try:
                value = eval_expression(valuestr, vars)

                result += string[p:p2]

                result += str(value or "")
                break

            except lrparsing.TokenError as ex:
                # example string:antes${text2.match("[0-9]{3,6}")}
                # existe uma "}" dentro da expressao!, mas p3 vai apontar para a 1º
                # isso vai provocar um erro de lrparsing. entao, vamos procurar outra "}"
                # e encontramos a 2º, que vai funcionar
                # se nao houver mais "}", nao vamos avaliar a expressao
                # nota: aqui apenas apanhamos erros de parse- lrparsing.TokenError
                # nao apanhamos erros de variaveis ou funcoes inexistentes
                p3 = string.find("}", p3+1)

        if p3 == -1:
            break

        # continue after the "}"
        p = p3+1

    result += string[p:]
    return result

# auxiliary function to join dicts. priority for latest
# print(_join_dicts({"a": 10, "b": 20}, {"b": 30, "d": 40})) ==> {'a': 10, 'b': 30, 'd': 40}
def join_dicts(*dicts):
    if len(dicts) == 0: return {}

    result = dicts[0]
    for d in dicts[1:]:
        result = {**result, **d}
    return result
