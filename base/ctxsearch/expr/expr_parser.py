import operator, re
import logging

import threading

# About lrparsing
# http://lrparsing.sourceforge.net/doc/html/
# https://github.com/wks/lrparsing3/tree/master/doc/examples
from lrparsing import Grammar, Prio, Ref, Token, Tokens, TokenRegistry, Keyword, List, Opt

# Functions and methods to be available in expression evaluation.
# Other modules can add more items here.
# Both are dict from name to a function reference
# For methods, first arg of function will be the object
FUNCTIONS = {}
METHODS = {}

# avaliação de operadores
_BINARY_OP = {
    '+': operator.add,
    '-': operator.sub,
    '*': operator.mul,
    '/': operator.floordiv,
    '%': operator.mod,
    '**': operator.pow,
    '==': operator.eq,
    '!=': operator.ne,
    '>': operator.gt,
    '<': operator.lt,
    '>=': operator.ge,
    '<=': operator.le,
    'and': operator.and_,
    'or': operator.or_,
}
_eval_binary = lambda n: _BINARY_OP[n[2]](n[1], n[3])

_UNARY_OP = {
    '-': operator.neg,
    'not': operator.not_
}
_eval_unary = lambda n: _UNARY_OP[n[1]](n[2])

# use to store variables used in the grammar
# Since I don't know how to use a different value for each parser,
# I use a thread local var to store then
_LOCAL = threading.local()

# evalueates expression, using values in dict VARIABLES. Returns result
def eval_expression(exprstr, VARIABLES):
    # logging.debug(f"eval_expression {exprstr} vars={VARIABLES}")

    # This won't work if one expression eval function calls eval_expression again!
    # if I want that, LoCAL.vars should be a stack...
    _LOCAL.vars = VARIABLES
    return _ExprGrammar.parse(exprstr, _ExprGrammar.eval_node)

# Define the Grammar class inside the function, so that I can use different set of VARS
# Also, could choose different start points... but using always "expr"
class _ExprGrammar(Grammar):
    # Tokens
    class T(TokenRegistry):
        number_int = Token(re='[0-9]+')
        number_int["eval"] = lambda n: int(n[1], 10)

        number_float = Token(re='[0-9]+\.[0-9]*')
        number_float["eval"] = lambda n: float(n[1])

        # uma string que suporta escape de uma aspa ou de um barra!
        string = Token(re=r'"(?:[^"\\]|\\"|\\\\)*"')
        # in eval, have to remove the escaping \
        string["eval"] = lambda n: re.sub(r'\\([\\"])', '\\1', n[1][1:-1])

        identifier = Token(re="[A-Za-z_][A-Za-z0-9_]*")
        identifier["eval"] = lambda n: n[1]

    # regras

    expr = Ref("expr")

    # operadores aritmeticos

    minus_op = '-' >> expr
    minus_op["eval"] = _eval_unary

    pow_op = expr >> "**" >> expr
    mul_op = expr << Tokens('* / %') <<  expr
    mul_op["eval"] = _eval_binary

    add_op = expr << Tokens('+ -') <<  expr
    add_op["eval"] = _eval_binary

    # comparacoes
    gt_op = expr >> '>' >> expr
    gt_op["eval"] = _eval_binary

    lt_op = expr >> '<' >> expr
    lt_op["eval"] = _eval_binary

    ge_op = expr >> '>=' >> expr
    ge_op["eval"] = _eval_binary

    le_op = expr >> '<=' >> expr
    le_op["eval"] = _eval_binary

    eq_op = expr >> '==' >> expr
    eq_op["eval"] = _eval_binary

    ne_op = expr >> '!=' >> expr
    ne_op["eval"] = _eval_binary

    in_op = expr >> Keyword('in') >> expr
    in_op["eval"] = lambda n: n[1] in n[3] # not an operator...

    # logicos
    true_ = Keyword("True")
    true_["eval"] = lambda n: True

    false_ = Keyword("False")
    false_["eval"] = lambda n: False

    not_op = Keyword('not') >> expr
    not_op["eval"] = _eval_unary

    and_op = expr >> Keyword('and') >> expr
    and_op["eval"] = _eval_binary

    or_op = expr >> Keyword('or') >> expr
    or_op["eval"] = _eval_binary

    # outras construcoes

    brackets = '(' + expr + ')'
    brackets["eval"] = lambda n: n[2]

    vector = '[' + List(expr, ',', 0) + ']'
    vector["eval"] = lambda n: n[2:-1:2]

    # nao posso atribuir directamente var=T.identifier! uso artificio de "* 1", como vi na documentacao
    variable = T.identifier * 1

    def variable_eval(n):
        if n[1] in _LOCAL.vars:
            return _LOCAL.vars[n[1]]
        raise Exception(f"Variable {n[1]} not found")
    variable["eval"] = variable_eval

    def function_call_eval(n):
        if n[1] in FUNCTIONS:
            # n will be [xx,fun_name,'(', arg1, ',', arg2... ')'
            # *n[3:-1:2] will take all the args
            return FUNCTIONS[n[1]](*n[3:-1:2])
        raise Exception(f"Function {n[1]} not found")
    function_call = T.identifier + '(' + List(expr, ',', 0) + ')'
    function_call["eval"] = function_call_eval

    # TODO podia ter tambem um operador "?." estilo kotlin? :-)
    method_call = expr + '.' + T.identifier + Opt('(' + List(expr, ',', 0) + ')')
    def method_call_eval(n):
        key = (type(n[1]), n[3])
        if key in METHODS:
            # n will be [xx,fun_name,'(', arg1, ',', arg2... ')'
            # *n[3:-1:2] will take all the args
            return METHODS[key](*(n[1:2] + n[5:-1:2]))
        raise Exception(f"Method {n[3]} not found")
    method_call["eval"] = method_call_eval

    # operator precedente based on python - https://www.programiz.com/python-programming/precedence-associativity
    expr = T.number_int | T.number_float | T.string | true_ | false_ | \
           method_call | function_call | variable | brackets | vector | Prio(
        pow_op,
        minus_op, not_op,
        mul_op, add_op,
        eq_op, ne_op, gt_op, lt_op, ge_op, le_op, in_op,
        not_op,
        and_op,
        or_op
    )

    START = expr

    # How it's evaluated.
    @classmethod
    def eval_node(cls, n):
        return n[1] if not "eval" in n[0] else n[0]["eval"](n)


if __name__ == "__main__":
    # USE: test.sh ctxsearch.evalexpr '1+2'

    import time

    from optparse import OptionParser
    parser = OptionParser()
    # parser.add_option("--string", '-s', action="store_true")
    # parser.add_option("--expr", '-e', action="store_true")
    (options, args) = parser.parse_args()

    METHODS = {
        (str,"len") : len,
    }
    FUNCTIONS = {
        'hello': lambda x: 1000 * x,
        'max': max,
        #'len': len,
        'noargs': lambda: 142857,
    }

    myvars = {"a": 1, "b": 10, "c": 100}

    for exprstr in args:
        t0=time.time()

        res = eval_expression(exprstr, myvars)
        delta= time.time() - t0

        print("%s = %s" % (exprstr, res))
        print(f"type = {type(res)}")

        print(f"{delta} seconds")
