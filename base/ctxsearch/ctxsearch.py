
import logging
import os, copy
import threading
import traceback

import gi
import html
import signal
import time

gi.require_version('Gtk', '3.0')
from gi.repository.GdkPixbuf import Pixbuf
from gi.repository import Gtk, Gdk,GObject, GLib

from .floating import floating
from .config import Config
from .etc import gtk_util
from .manage_plugins import ManagePlugins
from .sectioned_menu import SectionedMenu
from .actions import Action
from .globals import DATA_DIR, STATE_DIR, CONFIG_DIR

# The icons that shows in the menu!
# That should not be fixed but based on the screen resolution... how?
MENU_ICON_SIZE = 20

# miliseconds
# if -1, menus will never be destroyed
# and can be activated by sigusr1
DESTROY_MENU_INTERVAL = -1 ## 5000

TIME_THRESHOLD = 0.2
# TODO should show message?
def plugin_measure_time(plugin, func, *args):
    import time
    try:
        t0 = time.time()
        return func(*args)
    finally:
        delta = time.time() - t0
        if delta > TIME_THRESHOLD:
            logging.debug(f"Plugin warning: {plugin.name} {func.__name__} took {delta} seconds")

# def context2str(ctx):
#     str = ", ".join([f"{k}={v}" for k,v in ctx.items() if k != "text"])
#     if str:
#         str += ", "
#     if os.getenv("CTXSEARCH_DEBUG"):
#         str += f'text={ctx["text"]}'
#     else:
#         str += f'text.len={len(ctx["text"])}'
#     return str

# The main ctxsearch control, manages plugins, detects text selections, prepares and opens the menu
class CtxSearch:
    def __init__(self):
        self.config = Config()

        try:
            self.config.read()
        except Exception as ex:
            # we will show the error to the user on the first text selection
            logging.exception("first read config: {ex}")

        # active plugins ordered by priority
        self.pluginman = ManagePlugins(self)

        self.pluginman.load()

        self.floating = floating.Floating(os.path.join(DATA_DIR, "floating.png"), self.floating_state_changed, self.floating_clicked)

        signal.signal(signal.SIGUSR1, self.on_sigusr1)

        self._destroy_menu_timer = None

    def on_sigusr1(self, signum, stack):
        logging.debug("on_sigusr1")

        # do it on the UI thread:
        GLib.idle_add(self._activate_menu)

    def error_message(self, msg):
        gtk_util.message_dialog(msg)

    # the  top menu at pos 0, and all other submenus
    menus = []

    # if the menu is active now
    menu_is_active = False

    _gtk_thread=None

    # add to menu items related to the pair action+plugin
    # action é um obj Action
    def _add_menu_section(self, menu, plugin, action_index, action):
        # a unique section name for this action/plugin pair:
        section_name = f"{action_index}/{plugin.name}"
        # logging.debug(f"section_name {section_name}")
        menu.add_section(section_name)

        def add_item(section_name, menuitem):
            # logging.debug(f"__add_item... plugin={plugin.name}, thread={threading.currentThread()==self._gtk_thread}")

            if menu in self.menus or self.menu_is_active:
                # logging.debug(f"add_item!!! for section_name {section_name}")

                # TODO: should do this??
                # TODO metodo _apply_menuitem_defauklts(menuitem, action) ??
                if not menuitem.get_label() and "label" in action:
                    menuitem.set_label(action.get("label", self.ctx))

                # TODO this doesn't work anymore, MenuItem has no set_image method. Gtk.ImageMenuItem has(?)
                # if not menuitem.get_image() and "icon" in action:
                #     logging.debug("_adding icon")
                #     pixbuf = Pixbuf.new_from_file_at_size(action["icon"], MENU_ICON_SIZE, MENU_ICON_SIZE)
                #     img = Gtk.Image()
                #     img.set_from_pixbuf(pixbuf)
                #     menuitem.set_image(img)
                #
                #     # if I don't call this, the image will only appear if gnome is configured to show images in menus!
                #     # But I reallly want the images!!!
                #     menuitem.set_always_show_image(True)

                # logging.debug(f"LABEL={menuitem.get_label()}")

                menu.add_menuitem(section_name, menuitem)
            else:
                logging.debug("menu invalidated")

        try:
            plugin_measure_time(plugin, plugin.prepare_menu, self.ctx, action, lambda menuitem: add_item(section_name, menuitem))
        except Exception as ex:
            menu.add_menuitem(section_name, self.create_error_menuitem(action.dict, plugin, ex))

    # creates a menu item with info about an error that happened during menu initialization
    # @param yaml_element The config action element that caused the error
    # @param plugin if not None, the plugin that produced the error
    # @param exception The error
    def create_error_menuitem(self, yaml_element, plugin, exception):
        logging.debug(f"create_error_menuitem error={exception} {traceback.format_exc()}")

        msg = "Error"

        try:
            # read the linenr of the action, using ruamel yaml
            # this is undocumented feature?
            # logging.debug(f"linecol={yaml_element.dict._yaml_line_col}")
            line = yaml_element._yaml_line_col.line # start in 0?
            msg += f" in action line {line+1}"
        except:
            # logging.debug(f"linecol=error")
            pass

        if plugin:
            msg += f",  plugin {plugin.name}"

        msg += ": "

        msg += str(exception)

        lab = Gtk.Label()
        lab.override_color(Gtk.StateFlags.NORMAL, Gdk.RGBA(1.0, 0.0, 0.0, 1))
        lab.set_markup(f"<small>{msg}</small>")

        pb = Pixbuf.new_from_file_at_size(os.path.join(DATA_DIR, "menuitem_error.png"), 20,20)
        img = Gtk.Image()
        img.set_from_pixbuf(pb)

        box = Gtk.HBox()
        box.pack_start(lab, expand=False, fill=False, padding=0)

        mi = Gtk.ImageMenuItem()
        mi.set_image(img)
        mi.add(box)

        def clicked(mi):
            # TODO mostrar o stack trace?
            # ou abrir o editor na linha indicada??
            logging.debug("CLICKED!!!")

        mi.connect('activate', clicked)

        mi.show_all()

        return mi

    # adds the actions from list actions to given menu
    def _actions_to_menu(self, actions, menu):
        for index, element in enumerate(actions):
            if type(element) == str:
                # simple actions
                element = {element: None}

            if "separator" in element:
                # a menu separator
                section_name = menu.add_section(None)
                menu.add_menuitem(section_name, Gtk.SeparatorMenuItem())

            elif "submenu" in element:
                try:
                    if not "label" in element:
                        raise Exception("submenu should have label")

                    submenu = SectionedMenu()
                    self._menu_connect_control_enter(submenu)

                    self.menus.append(submenu)

                    # add the actions to the submenu
                    self._actions_to_menu(element["submenu"], submenu)

                    menuitem = Gtk.MenuItem(element["label"])
                    menuitem.set_submenu(submenu)
                    menu.add_menuitem(None, menuitem)

                except Exception as ex:
                    menu.add_menuitem(None, self.create_error_menuitem(element, None, ex))

            else:
                action = Action(element)

                try:
                    # call each plugin for this action
                    if action.verify_condition(self.ctx):
                        for plugin in self.pluginman.all():
                            # I moved this to a separate method because was having problem
                            # with section_name variable section_name declared inside the loop, that would
                            # have always the last value assigned to it...

                            self._add_menu_section(menu, plugin, index, action)
                except Exception as ex:
                    menu.add_menuitem(None, self.create_error_menuitem(action.dict, None, ex))

        return menu

    def _menu_connect_control_enter(self, menu):
        pass
        # def on_key_press(m, ev):
        #     KEY_ENTER = 36
        #     logging.debug(f"menu k press key={ev.hardware_keycode} state{ev.state}")
        #     if ev.hardware_keycode == KEY_ENTER and ev.state & Gdk.ModifierType.CONTROL_MASK:
        #         logging.debug("firing again without control")
        #         self._menu_control_modifier = True
        #
        #         #ev2 = Gdk.Event.new(Gdk.EventType.KEY_PRESS)
        #         #ev2.hardware_keycode = KEY_ENTER
        #         ev2 = ev.copy()
        #         ev2.state = Gdk.ModifierType(0)
        #         #Gtk.main_do_event(ev2)
        #         menu.event(ev2)
        #
        # menu.connect("key-press-event", on_key_press)
        # #menu.connect("key-release-event", lambda x,ev: logging.debug(f"topmenu k release key={ev.hardware_keycode} state{ev.state}"))

    def _prepare_menus(self):
        logging.debug("_prepare_menus")
        self._menu_control_modifier = False

        topmenu = SectionedMenu()

        try:
            self._menu_connect_control_enter(topmenu)

            self.menus = [topmenu]

            # Can raise error here!
            self.config.read()

            for plugin in self.pluginman.all():
                try:
                    plugin_measure_time(plugin, plugin.menu_destroyed)
                except Exception as ex:
                    logging.exception(f"Error on plugin {plugin.name} menu_destroyed: {ex}")

            # check new plugins
            self.pluginman.load()

            self.ctx = self.floating.get_context()
            if os.getenv("CTXSEARCH_DEBUG"):
                logging.debug(f'ctx text={self.ctx["text"]}')

            if len(self.ctx["text"]) > self.config.get_int("max_text_len", default=10000):
                raise Exception("maximum text size exceeded")

            # each plugin can add attrs to context
            for plugin in self.pluginman.all():
                try:
                    plugin_measure_time(plugin, plugin.prepare_context, self.ctx)
                except Exception as ex:
                    topmenu.add_menuitem(None,
                                         self.create_error_menuitem(None, plugin, html.escape(str(ex))))

            actions = self.config.get("actions")

            # prepare top menu and all submenus if available
            self._actions_to_menu(actions, topmenu)

            self.menu_is_active = False

            # Will be called when user closes the menu
            # only for the top menu
            def menu_hidden(x):
                # if this was called, means the menu has been activated, which means the floating is hidden now
                logging.debug("menu_hidden")
                self.menu_is_active = False
                for plugin in self.pluginman.all():
                    try:
                        plugin_measure_time(plugin, plugin.menu_destroyed)
                    except Exception as ex:
                        logging.exception(f"Error on plugin {plugin.name} menu_destroyed: {ex}")

            topmenu.connect("hide", menu_hidden)

            if self._destroy_menu_timer:
                GLib.source_remove(self._destroy_menu_timer)

            # this will free the menu resources
            if DESTROY_MENU_INTERVAL >= 0:
                self._destroy_menu_timer = GLib.timeout_add(DESTROY_MENU_INTERVAL, self._destroy_menus)

            # # tried also this signals to detect when menu is popupped and closed
            # # only hide and deactivate does something
            # menu.connect("popped-up", lambda x: logging.debug(f"menuitem popped-up"))
            # menu.connect("activate-current", lambda x: logging.debug(f"menuitem activate-current"))
            # menu.connect("deactivate", lambda x: logging.debug(f"menuitem deactivate"))
            # menu.connect("hide", lambda x: logging.debug(f"menuitem hide"))
            # menu.connect("destroy", lambda x: logging.debug(f"menuitem destroy"))
            # menu.connect("cancel", lambda x: logging.debug(f"menuitem cancel"))

        except Exception as ex:
            # yaml error
            topmenu.add_menuitem(None, self.create_error_menuitem(None, None, html.escape(str(ex))))
            # nothing more
            return


    def floating_state_changed(self, floating_state):
        logging.debug(f"floating_state_changed {floating_state} thread={threading.currentThread()}")
        self._gtk_thread=threading.currentThread()

        if floating_state == floating.STATE_SELECTION_COMPLETE:
            self._prepare_menus()

    def _destroy_menus(self):
        logging.debug("_destroy_menus")

        self.menus = []
        # the floating is hidden, but the menu can have been activated by the user
        # in that case, plugins can still add options to it
        if not self.menu_is_active:
            for plugin in self.pluginman.all():
                try:
                    plugin_measure_time(plugin, plugin.menu_destroyed)
                except Exception as ex:
                    logging.exception(f"Error on plugin {plugin.name} menu_destroyed: {ex}")

        # should return False for the timer not to repeat
        return False

    def _activate_menu(self, event=None):
        if len(self.menus) == 0:
            return

        logging.debug("_activate_menu ok")

        self.menu_is_active = True

        if event:
            self.menus[0].popup(None, None, None, None, event.button, event.get_time())
        else:
            self.menus[0].popup(None, None, None, None, 1, time.time())

    # The user clicked the floating
    def floating_clicked(self, event):
        self._activate_menu(event)

    def _find_url_opener(self, action, force_external):
        # logging.debug(f"_find_url_opener {action}")
        plugin = None
        if force_external:
            plugin = self.pluginman.by_name(self.config.get(["url", "external"], default="default_browser"))

        if not plugin:
            plugin_name = action.get("url_target", default = self.config.get(["url", "target"], default = "webview"))
            plugin = self.pluginman.by_name(plugin_name)

        if plugin and plugin.does_open_url:
            return plugin

        return None

    # action if provided will give hints about how to open the url
    # what browser, window size etc
    # If force_external is True, we will use the plugin given in cfg url.external
    # even if a different one is given in the action
    def open_url(self, url, ctx=None, action=None, force_external=False):
        logging.debug(f"open_url url={url}, ..action={action}, force_external={force_external}")
        opener = self._find_url_opener(action, force_external)
        logging.debug(f"open_url {url} - using {opener.name}")

        if not opener:
            self.error_message("No plugin to open url")
        else:
            opener.open_url(url, ctx, action)

    # convenience function to setup a menuitem to open url if user clicks it
    # and force to open in external browser if user was pressing CTRL
    def prepare_click_to_open_url(self, menuitem, url, ctx, action):
        def on_mouse_release(mi, event):
            self._menu_control_modifier = bool(event.state & Gdk.ModifierType.CONTROL_MASK)
            logging.debug(f"on_mouse_release ctrl_modified={self._menu_control_modifier}")

        def on_activate(mi):
            logging.debug(f"on_activate ctrl_modified={self._menu_control_modifier}")
            self.open_url(url, ctx, action, force_external=self._menu_control_modifier)

        # The "mouse press event" event will be received before the "activate" event
        # and will allow us to know if user pressed ctrl key
        menuitem.connect("button-release-event", on_mouse_release)
        menuitem.connect("activate", on_activate)

    # 20220328 Used for instance if some plugin implements mouse press event on a menu item, but the menu is not automatically hidden
    # never used yet!!!
    def force_menu_close(self):
        logging.debug("force_menu_close")
        if len(self.menus) >0:
            self.menus[0].hide()
