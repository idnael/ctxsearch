import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk, Gdk, GLib
import cairo
import time, math, os, logging

from .event_detector import EventDetector

# possible values of state:

STATE_HIDDEN = "hidden"

# user started making a selection with the mouse, have produced a clipboard_change event
# but we are waiting for mouse up event
STATE_SELECTION_STARTED = "selection_started"

# waiting some time to show floating
# The context is prepared at this state
STATE_SELECTION_COMPLETE = "selection_complete"

STATE_SHOWING = "showing"

SOURCE_MOUSE = "mouse"
SOURCE_KEYBOARD = "keyboard"

# manages a borderless always on top transparent window that shows an image near the place where
# the mouse was when the selection was made.
class Floating:
    # The time the floating shows, in seconds
    DEFAULT_HIDE_TIME = 3
    HIDE_TIME = DEFAULT_HIDE_TIME

    # All DEFAULT_XX constants will be used as a default for the corresponding XX property, if not present in cfg file

    # The distance in pixels the mouse can move away from the floating icon, until it disappears.
    # TODO should be in inchs? and should be configurable...
    DEFAULT_HIDE_DISTANCE = 400
    HIDE_DISTANCE = DEFAULT_HIDE_DISTANCE

    # interval where I check mouse events etc. In miliseconds
    DEFAULT_TIMER_INTERVAL = 50
    TIMER_INTERVAL = DEFAULT_TIMER_INTERVAL

    # Percentage of time after which floating will start to became transparent until disapear.
    # A value of 1 means only to hide imediatly when time is finished
    DEFAULT_HIDE_FACTOR = 0.9
    HIDE_FACTOR = DEFAULT_HIDE_FACTOR

    # Minimum time the floating window shows, even if there is a mouse event or other event.
    DEFAULT_MINIMUM_TIME = 0.2
    MINIMUM_TIME = DEFAULT_MINIMUM_TIME

    # --> nao vou precisar mais disto ???
    # after a selection is detected, we wait this time before showing the floating window.
    # This is because some programs create continuos selection events while the user
    # is dragging the mouse. and it is better if the floating window only appear in the end.
    DEFAULT_SELECTION_DELAY = 0.150
    SELECTION_DELAY = DEFAULT_SELECTION_DELAY

    # When a text is selected, the floating window doesn't shows
    # exactly in the mouse position but at this vertical distance.
    # This is not to cover the text that is in the mouse position, and make the application least intrusive, if the user has not made ​​a selection without intention to perform an action.
    # -> em pixels?
    DEFAULT_SELECTION_DISTANCE = 20
    SELECTION_DISTANCE = DEFAULT_SELECTION_DISTANCE

    def __init__(self, image_file, state_cb, click_cb):
        self._state_callback = state_cb
        self._clicked_callback = click_cb

        # prepare the window
        self._window = Gtk.Window(type=Gtk.WindowType.POPUP)

        self._window.set_border_width(0)
        # Gdk.Screen object representation of the screen on which windows can be displayed and on which the pointer moves:
        screen = self._window.get_screen()

        # will use to get mouse pos on screen
        self.pointer = screen.get_display().get_device_manager().get_client_pointer()

        # Make the window transparent.
        # gets a Gdk.Visual object to use for creating windows with an alpha channel.
        visual = screen.get_rgba_visual()
        if visual != None and screen.is_composited():
            self._window.set_visual(visual)
            self._window.set_app_paintable(True)
            # This signal is emitted when a widget is supposed to render itself
            self._window.connect("draw", self._area_draw)
        else:
            logging.debug("Transparency not supported.")
            # O icon que uso é quase quadrado por isso pouco se nota se não ficar transparente.
            # Podia usar icons diferentes no caso de a transparencia nao ser suportada!

        eventbox = Gtk.EventBox()
        # This is to detect mouse clicks in the floating window. This will activate the menu!
        eventbox.connect("button-press-event", self._clicked)
        self._window.add(eventbox)

        img = Gtk.Image()
        img.set_from_file(image_file)
        eventbox.add(img)
        eventbox.show_all()

        # now launch a keydetector, to detect if the user presses a key
        # or do a mouse click. That should make the floating window
        # disappear.
        self._ev_detector = EventDetector()

        # start detecting clipboard events!
        # The primary selection is activated everytime the user selects text!
        clip = Gtk.Clipboard.get(Gdk.SELECTION_PRIMARY)
        clip.connect('owner-change', self._clipboard_changed)

        # this is where we put information about the event, which is used to produce the actions menu
        self._ctx = {}

        self._timer = None

        self._selection_source = None
        self._change_state(STATE_HIDDEN)

    # TODO add more settings, call this when config changes
    # Receives an hash that can have any of these values: selection_distance, ...
    # if not present, use a default
    def change_settings(self, sett):
        self.SELECTION_DISTANCE = sett.get("selection_distance", self.DEFAULT_SELECTION_DISTANCE)
        
    def get_context(self):
        return self._ctx

    def _change_state(self, newstate):
        logging.debug(f"change_state {newstate}")
        self._state = newstate
        self._state_callback(newstate)
        
    # This is necessay for the window transparency
    # Last argument cr is a cairo.Context object
    def _area_draw(self, widget, cr):
        cr.set_source_rgba(.2, .2, .2, 0)
        cr.set_operator(cairo.OPERATOR_SOURCE)  # replace destination layer
        cr.paint()  # A drawing operator that paints the current source everywhere within the current clip region
        cr.set_operator(cairo.OPERATOR_OVER)  # destination on top of source

    # returns global mnouse position as a tuploe x,y
    # --> mesmo valores que dados no KeyDetector?
    def _mouse_xy(self):
        # antes fazia assim, mas agora é deprecated
        # _, x, y, mods = self._window.get_screen().get_root_window().get_pointer()
        scr, x, y = self.pointer.get_position()

        return x, y

    # called when clipboard content changed. Will check other conditions, and maybe display the floating icon
    def _clipboard_changed(self, clipboard, event):
        # lê sincronamente o conteudo do clipboard...
        text = clipboard.wait_for_text() or ""

        if not text:
            logging.debug("clipboard_changed with empty text ignored")
            return

        if self._ev_detector.last_event_was_kbd():
            # in case it was already showing...
            self._hide_floating()

            self._selection_source = SOURCE_KEYBOARD
        else:
            self._selection_source = SOURCE_MOUSE

        # don't log text here, for privacy reasons
        logging.debug(f"clipboard_changed source={self._selection_source}")

        self._ev_detector.reset_event()

        # add time here?
        self._ctx = {"text": text}

        # There are some applications that produce several clipboard events while
        # the user is grabbing the mouse to make the selection.
        # So we wait some time before showing the floating icon.
        self._change_state(STATE_SELECTION_STARTED)

        if not self._timer:
            # start the timer that will be use to show and hide the floating
            # The timer will be removed later
            self._timer = GLib.timeout_add(self.TIMER_INTERVAL, self._timer_func)

    # this will be called repeated after clipboard is changed
    def _timer_func(self):
        #logging.debug(f"__timer_func, state={self._state}")
        if self._state == STATE_SELECTION_STARTED:
            if self._ev_detector.had_new_event():
                logging.debug("hidding before showing, by event")
                self._hide_floating()

            elif not self._ev_detector.is_mouse_down():
                logging.debug("mouse up!")
                self._change_state(STATE_SELECTION_COMPLETE)

                # # posicao inicial...?
                mouse_x, mouse_y = self._mouse_xy()

                self._ctx["time"] = time.time()
                self._ctx["x"] = mouse_x
                self._ctx["y"] = mouse_y

        elif self._state == STATE_SELECTION_COMPLETE:
            if self._selection_source == SOURCE_MOUSE and time.time() > self._ctx["time"] + self.SELECTION_DELAY:
                # this will change stete to STATE_SHOWING
                self._show_floating()

        elif self._state == STATE_SHOWING:
            mouse_x, mouse_y = self._mouse_xy()

            # -> floatging_x? podia usar o ctx["x"]
            distance = math.sqrt((mouse_x - self.floating_x) * (mouse_x - self.floating_x) + (
                        mouse_y - self.floating_y) * (mouse_y - self.floating_y))

            # combine the elapsed time and the distance in one factor
            k = distance / self.HIDE_DISTANCE + (time.time() - self._ctx["time"]) / self.HIDE_TIME

            if k > 1:
                logging.debug("hiding by time or distance")
                self._hide_floating()

            elif k > self.HIDE_FACTOR:
                # fade out slowly
                self._window.set_opacity((1 - k) / (1 - self.HIDE_FACTOR))

            else:
                self._window.set_opacity(1)

            # o teste do MIN_TIME é para evitar que ele detecte o proprio evento que deu origem a selecao??
            if time.time() - self._ctx["time"] > self.MINIMUM_TIME:
                if self._ev_detector.had_new_event():
                    logging.debug("hiding by event")
                    self._hide_floating()

        return True

    # Show the floating! prepares the menu
    def _show_floating(self):
        logging.debug("show_floating")

        self.delayed_to_show = False
        w, h = self._window.get_size()

        # if selection direction is down, show the floating win below the mouse. Else, show above
        if self._ctx["y"] > self._ev_detector.mouse_down_y:
            delta_y = self.SELECTION_DISTANCE
        else:
            delta_y = - self.SELECTION_DISTANCE

        # the floating position:
        self.floating_x = self._ctx["x"] - w / 2
        self.floating_y = self._ctx["y"] + delta_y - h / 2

        self._window.move(self.floating_x, self.floating_y)
        self._window.set_opacity(1)

        self._change_state(STATE_SHOWING)

        self._window.show()

    # hide the floatigng window is visible. Cancel the timerfunc
    def _hide_floating(self):
        self._window.hide()
        logging.debug("hide_floating")

        if self._timer:
            # stop it
            GLib.source_remove(self._timer)
            self._timer = None

        self._change_state(STATE_HIDDEN)

    # the floating window was clicked!
    def _clicked(self, widget, event):
        logging.debug("Clicked")
        self._clicked_callback(event)
        return True


if __name__ == "__main__" :

    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    # activate logs!
    from ctxsearch.etc.mylog import *

    logging.debug("OLA DEBUG")

    floating = None

    def state_changed(state):
        print("STATE=%s, text=«««%s»»»" % (state, floating and floating.get_context().get("text")))

    def clicked(ev):
        print("CLICKED!!! text=«««%s»»»" % floating.get_context().get("text"))

    floating = Floating(state_changed, clicked)

    Gtk.main()

