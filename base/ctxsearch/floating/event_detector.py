#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2014 Daniel Carvalho <idnael@pegada.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.

from . import pyxhook
import logging

# TODO react to mouse wheel also?

LOG_EVENTS = False

class EventDetector:
    _is_mouse_down = False
    _last_event_was_kbd = False
    _had_new_event = False

    def __init__(self):
        self.hm = pyxhook.HookManager()
        self.hm.HookKeyboard()
        self.hm.HookMouse()

        # setup to receive events from hm:
        self.hm.KeyDown = self._event_key_down
        self.hm.MouseAllButtonsDown = self._event_mouse_down
        self.hm.MouseAllButtonsUp = self._event_mouse_up
        # TODO: should also detect mouse wheel events. how? - 201906 para quê??

        self.hm.start()

    def is_mouse_down(self):
        return self._is_mouse_down

    # apenas considera mouse_up e kbd
    def last_event_was_kbd(self):
        return self._last_event_was_kbd

    def had_new_event(self):
        return self._had_new_event

    def reset_event(self):
        self._had_new_event = False

    def destroy(self):
        self.hm.cancel()

    # Methods to receive events received from pyxhook:
    def _event_mouse_down(self, event):
        if LOG_EVENTS: logging.debug("event_mouse_down")

        # ---> nao devia ser preciso...
        self.mouse_down_y = event.Position[1]
        self._is_mouse_down = True
        self._had_new_event = True
        self._last_event_was_kbd = False

    def _event_mouse_up(self, event):
        if LOG_EVENTS: logging.debug("event_mouse_up")
        self._is_mouse_down = False
        self._last_event_was_kbd = False

    def _event_key_down(self, event):
        # detect if keyboard key was pressed, ignoring modifier keys

        # I made this list by testing. They are defined in pyxhook.py??
        MODIFIERS = set(["Control_L", "Control_R", "Shift_L", "Shift_R", "aLT_l"])

        is_modifier = event.Key in MODIFIERS

        if LOG_EVENTS:
            logging.debug(f"event_key_down is_modifier={is_modifier}")
            # for k in ['Ascii', 'Key', 'KeyID', 'MessageName', 'ScanCode']:
            #    print(f"{k}={getattr(event,k)}")
            # print()

        if not is_modifier:
            self._last_event_was_kbd = True
            self._had_new_event = True

if __name__ == "__main__" :
    # 20211220
    # faz logs de eventos de mouse up down, kbd e clipboard changed
    # Correr assim:
    # ./test.sh ctxsearch.floating.event_detector

    import gi
    gi.require_version('Gtk', '3.0')

    from gi.repository import Gtk, Gdk, GLib

    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    from ctxsearch.etc.mylog import log_to_console
    log_to_console()

    log_to_console()

    LOG_EVENTS = True

    # just for the logs
    EventDetector()

    def clip_changed(clipboard, event, clipname):
        # lê sincronamente o conteudo do clipboard...(?)
        text = clipboard.wait_for_text() or ""
        logging.debug(f"clipboard_changed {clipname}: «{text}»")

    # See the FreeDesktop Clipboard Specification - http://www.freedesktop.org/Standards/clipboards-spec
    # for a detailed discussion of the “CLIPBOARD” vs. “PRIMARY” selections
    # under the X window system. On Win32 the #GDK_SELECTION_PRIMARY clipboard is essentially ignored.)
    for clipname in ["SELECTION_PRIMARY", "SELECTION_CLIPBOARD", "SELECTION_SECONDARY"]:
        Gtk.Clipboard.get(getattr(Gdk, clipname)).connect('owner-change', clip_changed, clipname)

    Gtk.main()



