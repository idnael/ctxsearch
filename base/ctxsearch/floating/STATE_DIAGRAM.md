```mermaid
graph TD
A[\Hidden/]
B[\Sel. started/]
C[\Sel. completed/]
D[\Showing/]
B -- keyboard --> A
B -- mouse up --> C
C -- time selection delay --> D
D -- time or distance --> A
D -- keyboard --> A
Start --> A
all -- clipboard changed --> B
```

