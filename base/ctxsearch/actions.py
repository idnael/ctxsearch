import re
import logging
from .expr.expr import *

# encapsulates a action element of the config
# attribute "dict" contains the original ruamel.yaml object
class Action:
    def __init__(self, actiondict):
        self.dict = actiondict

    def __contains__(self, key):
        return key in self.dict
    def __getitem__(self, key):
        return self.get(key)

    # Gets the name of a action property
    # vars é um ou mais dict que serao usados para substituicao de expressoes dentro do valor
    # If prop is not found, returns default. Novariable substitution is done in the default value
    def get(self, key, *vars, default=None):
        try:
            if key not in self.dict:
                return default

            v = self.dict[key]
            if type(v) == str:
                return replace_vars(v, join_dicts(*vars))
            else:
                return v
        except Exception as ex:
            raise Exception(f"Error getting {key}:{ex}")

    # checks if action condiions results to True applied with the given vars
    # If action has a condition with error, this method will raise exception
    # EXAMPLES:
    # ctx = {"text":"ola mundo"}
    # print(Action({
    #     "url":"http://search.com/q=${text.urlquote}",
    #     "not": {"text.len.gt":222}
    # }).verify(ctx)) ## TRUE
    # print(Action({
    #     "url":"http://search.com/q=${text.urlquote}",
    #     "or": [{"text.len.lt":4}, {"text.len.ge":9}]
    # }).verify(ctx)) ## TRUE
    def verify_condition(self, *vars):
        vars_all = join_dicts(*vars)

        if "if" not in self.dict:
            return True

        condstr = self.dict["if"]
        try:
            result = eval_expression(condstr, vars_all)
            return bool(result)
        except:
            # 20211026 if condition throw an error, consider as a failure
            return False

    def __str__(self):
        return f"Action {self.dict}"

if __name__ == "__main__" :
    import time

    # activate logs!
    console = logging.StreamHandler()
    console.setFormatter(logging.Formatter('%(module)s %(levelname)-8s %(message)s'))
    logging.getLogger().addHandler(console)
    logging.getLogger().setLevel(logging.DEBUG)

    # x= _eval_expr({"text":"ola mundo"}, "textaa.urlquote")
    # print(x)
    #
    # print(_replace_vars("echo ${text} xxx ${textxx}", {"text":"ola mundo"}))

    ctx = {"text":"ola x127215 mundo", "a":123}
    # ctx = {"text": "http://ola"}
    a = Action({
        "url":'http://search.com/q=${text.match("x(\\\\d+)")}, a=${a}, a=${a}, a=${a}',
        "ola": '''antes${text2.match("[0-9]{3,6}")}''',
        #"if": 'len(text)> 122 or text=="ola"',
        "if": 'match(text, "^(https?:|www\\\\.)")'
    })

    t0=time.time()

    #print(a.get("url", ctx, {"texta":"ISTO!"}, default="coisa"))
    print(a.get("ola", ctx, {"texta": "ISTO!"}, default="coisa"))

    delta= time.time() - t0

    # print(a.verify(ctx))
    print(f"{delta} seconds")
