# -*- coding: utf-8 -*-

#    Copyright (C) 2014 Daniel Carvalho <idnael@pegada.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.

# normal yaml
#import yaml

# preserve formatting and comments!
# ver https://stackoverflow.com/questions/7255885/save-dump-a-yaml-file-with-comments-in-pyyaml
# $ pydoc3 ruamel.yaml.YAML
# sobre yaml normal: https://yaml.readthedocs.io/en/latest/api.html
import ruamel.yaml
yaml = ruamel.yaml.YAML()

import os, shutil, sys
import logging

from .globals import DATA_DIR, CONFIG_DIR
from .expr.expr import *

# used when there is an error loading the config file
EMPTY_CONFIG = {}

class Config:
    # cfgerror_cb is callback to be called with message if there is a config file error
    # called should call read() after init
    def __init__(self):
        self.cfg_file = os.path.join(CONFIG_DIR, "actions.yaml")
        self.data = EMPTY_CONFIG

        self._config_time = None

    # reads the config file
    # return true if config was changed
    # raises some Exception if there is an yaml error
    def read(self):
        # if there is no config dir, creates a default configuration
        if not os.path.exists(CONFIG_DIR):
            os.makedirs(CONFIG_DIR)

        if not os.path.exists(self.cfg_file):
            shutil.copy(os.path.join(DATA_DIR, "default_actions.yaml"), self.cfg_file)

        if self._config_time and self._config_time == os.path.getmtime(self.cfg_file):
            return False

        # if there is an yaml error, we will keep previous config
        #######self.data = EMPTY_CONFIG

        logging.debug("Reading %s" % self.cfg_file)

        with open(self.cfg_file, "r") as f:
            self.data = yaml.load(f)
            # normal yaml: self.data = yaml.load(f, Loader=yaml.FullLoader)

        # if read sucess, set the config time, so don't need to read again
        self._config_time = os.path.getmtime(self.cfg_file)

        return True

    # gets a property value
    # path can be str or a array of str
    # example: cfg.get(["a","b"]) is the same as cfg.get("a")["b"]
    # If property value is a string, and vars are given, perform expression substitution in the value
    # Example:
    # YAML file has this:
    # searchfiles:
    #    editor: xed '${file}' +${linenr}
    # This call
    # config.get(["searchfiles","editor], {"file":"ola.txt", "linenr":20})
    # will return "xed 'ola.txt' +20"
    def get(self, path, *vars, default=None):
        if type(path) == str:
            return self.get([path], *vars, default=default)

        result = self.data
        for name in path:
            if name not in result:
                return default
            result = result[name]

        if vars:
            if type(result) == str:
                # no, eval expr
                return replace_vars(result, join_dicts(*vars))
            else:
                raise Exception("Cant use vars with non string cfg values")

        return result

    def get_int(self, path, *vars, default=None):
        value = self.get(path, *vars, default=default)
        if type(value) != int:
            raise Exception(f"Expecting int value: {value}")
        return value

    def write(self):
        with open(self.cfg_file, "wt") as f:
            yaml.dump(self.data, f)


if __name__ == "__main__" :
    cfg = Config()
    cfg.read()

    print(cfg.get(["url","xtarget"], default="coisa"))