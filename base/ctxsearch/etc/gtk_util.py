# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject

import sys

def message_dialog(message):
    dialog = Gtk.MessageDialog(None, Gtk.DialogFlags.MODAL, Gtk.MessageType.INFO,
                               Gtk.ButtonsType.OK, "CtxSearch")
    dialog.props.secondary_text = message
    dialog.run()
    dialog.destroy()

