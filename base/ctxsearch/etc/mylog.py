import logging
#import logging.handlers
import os

# just import this module to use logging to console

def log_to_console():
    console = logging.StreamHandler()
    console.setFormatter(logging.Formatter('%(asctime)s %(module)s %(levelname)-8s %(message)s'))
    logging.getLogger().addHandler(console)
    logging.getLogger().setLevel(logging.DEBUG)

    class MyFilter(logging.Filter):
        def filter(self, record):
            return record.pathname and record.pathname.lower().find("ctxsearch") != -1

    # 20211011 don't show logs from requests and oauth lib...
    console.addFilter(MyFilter())

def log_to_file(log_file):
    # to file:
    #log_file = os.path.join(os.getenv("HOME"), "_ctxsearch.log")
    handler = logging.FileHandler(log_file)
    handler.setFormatter(logging.Formatter('%(asctime)s %(module)s %(levelname)-8s %(message)s'))
    logging.getLogger().addHandler(handler)
