# -*- coding: utf-8 -*-

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GdkPixbuf

from gettext import gettext as _
import os

from . import globals


class AboutWindow:

    def __init__(self):
        dialog = Gtk.AboutDialog()

        # TODO Arranjar outra forma de mostrar isto...
        # if not context.has_language_support():
        #    description += "Please install LangId library for language detection support!\n"

        infos = {
            "program_name": "CtxSearch",
            "logo" : GdkPixbuf.Pixbuf.new_from_file(os.path.join(globals.DATA_DIR,"app_icon.png")),
            "version": globals.VERSION,
            "comments": globals.DESCRIPTION,
            "copyright": globals.AUTHOR
            # "website": globals.WEBSITE,
        }

        dialog.set_authors(["Daniel P. Carvalho <idnael@pegada.net>"])

        for prop, val in infos.items():
            dialog.set_property(prop, val)

        dialog.connect("response", self.destroy)
        dialog.show_all()

    def destroy(self, dialog, response):
        dialog.destroy()
        AboutWindow.__instance = None



if __name__ == "__main__" :

    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    w = AboutWindow()
    Gtk.main()
