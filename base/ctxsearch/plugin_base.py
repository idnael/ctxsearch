
# Plugins should subclass this
class PluginBase:
    def __init__(self, ctxsearch):
        self.ctxsearch = ctxsearch
        self.config = ctxsearch.config

    # A list of plugin names that this plugin depends
    # These plugins must be installed, and will be called before this one.
    dependencies=[]

    # Functions to be added to evaluate expressions
    # A dict from string function name to a funtion
    action_functions = {}

    # Methods to be added to evaluate expressions
    # A dict from pair object type+method name to a funtion
    action_methods = {}

    # if this plugin has a working open_url method
    does_open_url = False

    # will contains instances of CtxSearch and Config
    ctxsearch=None
    config=None

    # The plugin name. When plugin is loaded, value will be overriden
    # with file name without ".py"
    name = ""

    # chamada quando é feito uma selecao.
    # Pode ser usado para adicionar nova informacao ao ctx.
    # Essa informacao pode ser usado depois pelo filter_action_tree ou outros metodos, tambem de outros modulos.
    # Podera usada por exemplo para filtrar actions com base em condicoes, pelo modsyntax
    # Nao tem valor de retorno
    def prepare_context(self, ctx):
        pass

    # Usado para criar menu items com base no ctx e tree
    # can raise Exception if there is an error in action
    # se o plugin estiver interessado, deve chamar o callback
    # dando um objecto Gtk::MenuItem
    # O plugin pode ser chamado sincronamente ou assincronamente
    # pode ser chamado 0 ou mais vezes
    # To remove an item, destroy it
    # 20211002 action é um objecto Action
    def prepare_menu(self, ctx, action, callback):
        pass

    # informs the plugin that the menu was closed
    # so if was doing background work from prepare_menu, should stop it
    def menu_destroyed(self):
        pass

    # plugin that handle urls should implement this
    # ctx and action can be used to find extra information on how to open the url, like x,y, icon..?
    # can raise Exception if there is an error in action
    def open_url(self, url, ctx, action):
        pass

    # NAO: a set of default actions that should be added to the tree when the plugin is first loaded
    # It should be a string, to be appended to user config file
    # SIM: Called the first time the plugin is loaded, the plugin can
    # modify config to add default settings and actions
    # Method should return True if modified the config, so it can be saved
    def add_default_config(self, config):
        pass

