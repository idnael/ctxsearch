import os

# info about system dirs
# https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
DATA_DIR = "/usr/share/ctxsearch/data"
PLUGIN_DIR = "/usr/share/ctxsearch/plugins"

AUTHOR = "Copyright © Daniel P. Carvalho <idnael@gmail.com>"
#WEBSITE = "http://idnael.pegada.net/ctxsearch"
VERSION = "0.20"

DESCRIPTION = "Do actions on selected text."

CONFIG_DIR = os.getenv("CTXSEARCH_CFGDIR") or os.path.join(os.getenv("HOME"), ".config", "CtxSearch")

STATE_DIR = os.path.join(os.getenv("HOME"), ".local", "share", "CtxSearch")

