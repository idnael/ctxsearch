import logging
import random
from bisect import bisect_right

import gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GObject, GLib

# A menu divided in sections. Sections borders are not visible to user.
# Items from the same section will be placed together, independent or the order they are added.
#
# Sections are placed in the order they were added, from top to bottom.
# Menuitems from each sections are placed in the order they were added
class SectionedMenu(Gtk.Menu):
    def __init__(self):
        Gtk.Menu.__init__(self)

        # ordered list of session
        self.section_name_to_index = {}

        self.menuitem_to_section_index = {}

        # always ascending order
        self.section_sizes = []

    # returns the section_name
    def add_section(self, section_name):
        if section_name == None:
            # generate a unique
            section_name = f"__{random.randint(0, 1000000)}"
            while section_name in self.section_name_to_index:
                section_name = f"__{random.randint(0, 1000000)}"

        if section_name in self.section_name_to_index:
            raise Exception(f"Repeated section {section_name}")

        index = len(self.section_name_to_index)
        self.section_name_to_index[section_name] = index
        self.section_sizes.append(0)

        return section_name

    def add_menuitem(self, section_name, menuitem):
        if section_name == None:
            # a single section just for this item
            section_name = self.add_section(None)

        menuitem.show()

        if len(self.get_children()) != sum(self.section_sizes):
            logging.error("Inconsistencia!")

        section_index = self.section_name_to_index[section_name]

        menu_index = sum(self.section_sizes[0:section_index+1])

        self.insert(menuitem, menu_index)

        self.section_sizes[section_index] += 1

        self.menuitem_to_section_index[menuitem] = section_index

        # detect if a menuitem is destroyed
        menuitem.connect("destroy", self._menuitem_destroyed)

    # def add_section_space(self):
    #     section_name = "_space"
    #     self.add_section(section_name)
    #     #disp = Gdk.Display()
    #     disp = self.get_display()
    #
    #     logging.debug("_mrd disp=%s" % str(disp))
    #     screen = disp.get_default_screen()  # crash!!!
    #     logging.debug("_mrd screen=%s" % str(screen))
    #     print(screen.get_height_mm())
    #     print(screen.get_height())

    def _menuitem_destroyed(self, menuitem):
        self.remove(menuitem)

        section_index = self.menuitem_to_section_index[menuitem]

        # logging.debug(f"_destroyed section_index={section_index}")

        self.section_sizes[section_index] -= 1

if __name__ == "__main__":
    # Correr assim
    # ### cd ~/comp/CtxSearch/src
    # ### python3 -m ctxsearch.sectioned_menu
    # $ ./test.sh ctxsearch.sectioned_menu

    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)

    # activate logs!
    from .etc import mylog

    # create a new window
    window = Gtk.Window()
    window.set_size_request(200, 100)
    window.set_title("GTK Menu Test")
    window.connect("delete_event", lambda w, e: Gtk.main_quit())

    menu = SectionedMenu()

    def button_press(widget, event):
        print("button_press")
        if event.type == Gdk.EventType.BUTTON_PRESS:
            menu.popup(None, None, None, None, event.button, event.get_time())
            # Tell calling code that we have handled this event the buck
            # stops here.
            return True
        # Tell calling code that we have not handled this event pass it on.
        return False


    button = Gtk.Button(label="press me")
    # button.connect_object("event", self.button_press, menu)
    button.connect("button-press-event", button_press)
    button.show()

    window.add(button)
    window.show()

    # -----------------
    # PREPARE THE MENU ITEMS - TESTE1
    # menu.add_section("b")
    # menu.add_section("a")
    #
    # a0 = Gtk.MenuItem(label="a0")
    # menu.add_menuitem("a", a0)
    #
    # menu.add_menuitem("a", Gtk.MenuItem(label="a1"))
    # menu.add_menuitem("b", Gtk.MenuItem(label="b0"))
    # menu.add_menuitem("a", Gtk.MenuItem(label="a2"))
    # menu.add_menuitem("b", Gtk.MenuItem(label="b1"))
    #
    # def remove_timer():
    #     print("remove_timer")
    #     #a0.destroy()
    #     #menu.remove_all()
    #     a0.set_label("a0!!!!!")
    # GLib.timeout_add(2000, remove_timer)

    # TESTE 2
    a = menu.add_section(None)
    b = menu.add_section(None)
    c = menu.add_section(None)
    mic1 = Gtk.MenuItem(label="c1")
    mic1.connect("activate", lambda mi: print("activated c1"))
    mic1.connect("button-release-event", lambda mi,ev: print(f"button-release-event c1 state={ev.state}"))

    menu.add_menuitem(c, mic1)

    def add_more_timer():
        print("add_timer")
        menu.add_menuitem(a, Gtk.MenuItem(label="a1"))
    GLib.timeout_add(4000, add_more_timer)

    # -----------------


    Gtk.main()
