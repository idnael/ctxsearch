import logging
import traceback
import os, sys, re
import yaml

from .globals import PLUGIN_DIR, CONFIG_DIR, STATE_DIR
from .expr.expr_parser import FUNCTIONS, METHODS

# https://stackoverflow.com/questions/19053707/converting-snake-case-to-lower-camel-case-lowercamelcase
def camel(snake_str):
    first, *others = snake_str.split('_')
    return ''.join([first.lower(), *map(str.title, others)])

BASE_PLUGINS = ["language", "system_info"]

class ManagePlugins:
    def __init__(self, ctxsearch):
        self.ctxsearch = ctxsearch
        self.config = ctxsearch.config

        self.state_file = os.path.join(STATE_DIR, "plugins.yaml")
        self._read_state()

        self._rejected_plugin_names = set()
        self._plugins = []

        personal_folder = os.path.join(CONFIG_DIR, "plugins")
        os.makedirs(personal_folder, exist_ok=True)

        # plugins installed on system + plugins installed on user home
        self.import_folders = [PLUGIN_DIR, personal_folder]

        sys.path += self.import_folders

        self.load()

        # for the sandbox:
        self.plugin_names_errors = set()

    def all(self):
        return self._plugins

    # if plugin with name exists, return it. Else, returns None
    def by_name(self, name):
        for p in self._plugins:
            if p.name == name:
                return p
        return None

    def _read_state(self):
        try:
            with open(self.state_file, "r") as f:
                self._state = yaml.load(f, Loader=yaml.FullLoader)
        except:
            self._state = {}

        # TODO cuidar caso em que ficheiro esta corrompido, por exemplo vazio, retornara None

        if not "loaded_plugins" in self._state:
            self._state["loaded_plugins"] = []

    def _write_state(self):
        logging.debug(f"writing {self.state_file}")
        with open(self.state_file, "wt") as f:
            yaml.dump(self._state, f)

    # reload new plugins in available
    # suponho q plugins nunca se alteram. So podem aparecer novos, se por exemplo o user instalar um pacote novo
    # Tb suponho q nunca sao apagados -- podia?? TODO test removed or ignored plugins!!
    def load(self):

        disabled_names_str = self.config.get(["plugins", "disable"], default=None)
        if disabled_names_str:
            disabled_names = [n.strip() for n in disabled_names_str.split(",")]
        else:
            disabled_names = []

        logging.debug(f"__ignored_names={disabled_names}")

        for folder in self.import_folders:
            # process files sorted, to be deterministic
            for fname in sorted(os.listdir(folder)):

                # procura todos os "plugin_*.py" dentro do folder. Por exemplo, "plugin_hello_world.py"
                # devera conter uma classe HelloWorldPlugin que herda de PluginBase

                # ou entao numa pasta, para caso de plugin com dependencias
                # Por exemplo "plugin_webview/plugin_webview.py" contem "WebviewPlugin"

                PLUGIN_FILENAME_RE = re.compile(r'plugin_(.*)\.py')
                PLUGIN_FOLDER_RE = re.compile(r'plugin_(.*)')

                if PLUGIN_FILENAME_RE.match(fname):
                    m = PLUGIN_FILENAME_RE.match(fname)
                    plugin_name = m.group(1)
                    if plugin_name not in disabled_names:
                        self._load_plugin(plugin_name, os.path.splitext(fname)[0])

                elif os.path.isdir(os.path.join(folder, fname)) and PLUGIN_FOLDER_RE.match(fname):
                    m = PLUGIN_FOLDER_RE.match(fname)
                    plugin_name = m.group(1)

                    if plugin_name not in disabled_names:
                        self._load_plugin(plugin_name, os.path.splitext(fname)[0]+"."+os.path.splitext(fname)[0])

        self._filter_plugins()

    def _load_plugin(self, plugin_name, package_name):
        # logging.debug(f"_load_plugin plugin_name={plugin_name} package_name={package_name}")

        # see
        # https://stackoverflow.com/questions/67631/how-to-import-a-module-given-the-full-path

        class_name = camel(plugin_name)
        # capitalize first letter
        class_name = class_name[0:1].upper() + class_name[1:] + "Plugin"

        # if any([type(pp).__name__ == class_name for pp in self.plugins]):
        if plugin_name in [p.name for p in self._plugins]:
            # ja temos este plugin
            # logging.debug(f"_already have {plugin_name}")
            return

        if plugin_name in self._rejected_plugin_names:
            # ignore, we tried already
            # logging.debug(f"_already rejected {plugin_name}")
            return

        try:
            exec(f"from {package_name} import {class_name}")
            cls = eval(class_name)

            # create instance
            plugin = cls(self.ctxsearch)

            # TODO how to detect if function expects 1 or 2 args? NOT NEEDED!!!
            # Could use import inspect; if len(inspect.getargspec(aMethod1).args) ==2: ...
            for n, f in plugin.action_functions.items():
                FUNCTIONS[n] = f

            for n, f in plugin.action_methods.items():
                METHODS[n] = f

            plugin.name = plugin_name

            logging.debug(f"Created plugin {class_name}")
            self._plugins.append(plugin)

            # if there is a new plugin maybe other plugin that was rejected for failed dependency
            # now will work
            self._rejected_plugin_names.clear()

            if not plugin_name in self._state["loaded_plugins"]:
                logging.debug(f"Plugin first time {plugin_name}")

                self._state["loaded_plugins"].append(plugin_name)
                self._write_state()

                if plugin.add_default_config(self.config):
                    self.config.write()

                    # reread config -- TODO porque? poara nao ler depois...
                    try:
                        self.config.read()
                    except:
                        # should not happen because yaml we created is valid
                        pass

        except:
            logging.debug(f"Error loading plugin {plugin_name}, package={package_name}: {traceback.format_exc()}")
            self.ctxsearch.error_message(f"Error loading plugin {plugin_name}: {sys.exc_info()[1]}")
            self._rejected_plugin_names.add(plugin_name)

    # sort the plugins based on priority and dependencies, and removes
    # plugins with uninstalled dependencies
    def _filter_plugins(self):
        old = self._plugins.copy() # nao destroi original ainda

        # put first the base plugins. note: False is smaller than True
        old.sort(key=lambda p: p.name not in BASE_PLUGINS)

        new = []

        error_messages = []

        def process(x, reject_dep_names=[]):
            old.remove(x)
            for depname in x.dependencies:
                if depname in reject_dep_names:
                    error_messages.append(f"Rejecting {depname} and {x.name} - dependency loop!")
                    return False
                if not (any([p.name == depname for p in new])):
                    # ainda nao esta em new
                    find = [p for p in old if p.name == depname]
                    if not find:
                        error_messages.append(f"Rejecting {x.name} - non existing dep {depname}!")
                        return False
                    if not process(find[0], reject_dep_names + [x.name]):
                        return False
            new.append(x)
            return True

        while len(old) > 0:
            process(old[0])

        if error_messages:
            self.ctxsearch.error_message(error_messages[0])

        for p in self._plugins:
            if p not in new:
                # logging.debug(f"rejected {p.name}")
                self._rejected_plugin_names.add(p.name)

        self._plugins = new
