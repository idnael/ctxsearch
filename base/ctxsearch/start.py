#!/usr/bin/env python
# -*- coding: utf-8 -*-

#    Copyright (C) 2014 Daniel Carvalho <idnaed@pegada.net>
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.

import gi
gi.require_version('Gtk', '3.0')

import sys, os, psutil, shutil
import codecs
from optparse import OptionParser

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk,GObject, GLib

# Setup log before importing my other python files
from .etc.mylog import log_to_console
log_to_console()

from .ctxsearch import CtxSearch
from .globals import STATE_DIR, CONFIG_DIR, DATA_DIR

# If you are only interested in Ctrl-c finishing your application and you don't
# need special cleanup handlers, the following will perhaps work for you:
import signal
signal.signal(signal.SIGINT, signal.SIG_DFL)

# reads package html file, convert to txt
def help_text():
    import html2text

    help_file = os.path.join(DATA_DIR, "ctxsearch-help.html")

    f = codecs.open(help_file, "r", "utf-8")
    html = f.read()
    f.close()

    return html2text.html2text(html)

parser = OptionParser(usage=help_text())
parser.add_option("--stop", action="store_true", help="kill running instance")
parser.add_option("--start", action="store_true")
parser.add_option("--restart", action="store_true")
parser.add_option("--reset", action="store_true")

parser.add_option("--activate", action="store_true", help="explicitly activate the floating icon")

(options, args) = parser.parse_args()


pid_file = os.path.join(STATE_DIR, "pid")

running_pid = None
try:
    with open(pid_file, "r") as fd:
        running_pid = int(fd.read())
    proc = psutil.Process(running_pid)
    if 'ctxsearch.start' not in proc.cmdline():
        # it is a different process! ignore it
        running_pid = None
        os.remove(pid_file)
except:
    running_pid = None

if options.restart:
    options.start = True
    options.stop = True

if not options.start and not options.stop and not options.reset and not options.activate:
    # default
    options.start = True

if options.stop:
    if running_pid:
        print(f"Killing pid {running_pid}")
        os.kill(running_pid, 9)
        os.remove(pid_file)
        running_pid = None
    else:
        print("Not running")

if options.reset:
    print(f"Removing {STATE_DIR}")
    shutil.rmtree(STATE_DIR)

os.makedirs(STATE_DIR, exist_ok=True)

if options.start:
    if running_pid:
        print(f"Already running with pid {running_pid}. Use --stop to terminate the other instance")
        sys.exit(1)

    mypid = os.getpid()
    with open(pid_file, "w") as fd:
        fd.write(f"{mypid}\n")

    GLib.set_application_name('Context Search')
    GLib.set_prgname('ctxsearch')

    CtxSearch()

    Gtk.main()

if options.activate:
    if not running_pid:
        print("Not running")
        sys.exit(1)

    os.kill(running_pid, signal.SIGUSR1)