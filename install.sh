#!/bin/bash

op="$1"

if [ "$op" == "--remove" ]
then
  # undo files copied by --install
  sudo rm -rf /usr/bin/ctxsearch /usr/bin/clipboard_files /usr/share/ctxsearch /etc/xdg/autostart/CtxSearch.desktop

else
  # Preciso ser sudo por causa dos chown

  # go to folder where this script is
  cd `dirname "$0"`

  name=ctxsearch-0.20
  target=_deb/$name

  sudo rm -rf $target

  mkdir -p $target
  mkdir -p $target/DEBIAN
  mkdir -p $target/usr/{bin,/share/ctxsearch/{base,data,plugins}}
  mkdir -p $target/etc/xdg/autostart

  cp script/{ctxsearch,clip_files} $target/usr/bin/

  # os src python e data
  cp -rp {base,dependencies,plugins,data} $target/usr/share/ctxsearch

  # ficherios especiais debian
  cp -rp etc/debian.control $target/DEBIAN/control

  cp etc/CtxSearch.desktop $target/etc/xdg/autostart

  sudo chmod 0755 $target/usr/bin/{ctxsearch,clip_files}
  sudo chown root:root -R $target/

  if [ "$op" == "--install" ]
  then
    # clean old, same as remove
    sudo rm -rf /usr/bin/ctxsearch /usr/share/ctxsearch /etc/xdg/autostart/CtxSearch.desktop

    sudo cp -rp $target/usr $target/etc /

    sudo rm -rf $target

  else
    if [ "$op" == "--debian" ]
    then
      # this will create a deb file in _deb folder
      dpkg -b $target/

      sudo rm -rf $target

    fi
  fi
fi
